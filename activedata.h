#ifndef ACTIVEDATA_H
#define ACTIVEDATA_H

#include <QObject>

struct ActiveWorker {
    QString id;
    QString code;
    QString name;
};
struct ActiveBench {
    QString id;
    QString name;
};
struct ActiveDetail {
    QString id;
    QString name;
};

class ActiveData : public QObject
{
    Q_OBJECT
public:
    explicit ActiveData(QObject *parent = nullptr);
    QString m_selectedToodeID = "0";
    QString m_user_kood = "";
    QString m_user_id = "";
    // info about active data




signals:

public slots:
    QString getUserKood();
    void setUserKood(QString val);

    QString getUser_id() const;
    void setUser_id(const QString &user_id);

    QString getSelectedToodeID();
    void setSelectedToodeID(QString val);
};

#endif // ACTIVEDATA_H
