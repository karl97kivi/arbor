#include "model_entrylist.h"
#include "list_entry.h"

EntryListModel::EntryListModel(QObject *parent)
    : QAbstractListModel(parent), mList(nullptr)
{
}

int EntryListModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid() || !mList)
        return 0;

    // FIXME: Implement me!
    return mList->entries().size();
}

QVariant EntryListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() || !mList)
        return QVariant();

    const EntryItems item = mList->entries().at(index.row());
    switch(role) {
        case TimeRole:
            return QVariant(item.kellaaeg);
        case DateRole:
            return QVariant(item.kuupaev);
        case AmountRole:
            return QVariant(item.lisatud_kogus);
    }
    return QVariant();
}

QHash<int, QByteArray> EntryListModel::roleNames() const
{
    QHash<int, QByteArray> names;
    names[DateRole] = "kuupaev";
    names[TimeRole] = "kellaaeg";
    names[AmountRole] = "kogus";
    return names;
}

EntryList *EntryListModel::list() const
{
    return mList;
}

void EntryListModel::setList(EntryList *list)
{
    beginResetModel();
    if (mList)
        mList->disconnect(this);
    mList = list;
    emit dataChanged(createIndex(0,0), createIndex(20,0));
    endResetModel();
}
