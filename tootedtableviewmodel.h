#ifndef TOOTEDTABLEVIEWMODEL_H
#define TOOTEDTABLEVIEWMODEL_H

#include <QAbstractTableModel>
#include "dvtablelist.h"


class TootedTableViewModel : public QAbstractTableModel
{
    Q_OBJECT
    Q_PROPERTY(DVtableList *cList READ cList WRITE setCList NOTIFY cListChanged)
    Q_PROPERTY(QString selectedToode READ getSelectedToode WRITE setSelectedToode NOTIFY selectedToodeChanged)
    Q_PROPERTY(int alamIndex READ getAlamIndex WRITE setAlamIndex NOTIFY alamIndexChanged)

public:
    explicit TootedTableViewModel(QObject *parent = nullptr);
    enum TableRoles {
        TableDataRole = Qt::UserRole +1,
        HeadingRole,
        TitleRole,
        WidthRole
    };
    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    virtual QHash<int, QByteArray> roleNames() const override;
    Q_INVOKABLE QString getData(int row, QString par);

    void setSelectedToode(QString);
    QString getSelectedToode();
    int getAlamIndex();
    void setAlamIndex(int val);
    Q_INVOKABLE void setInitToode();
    Q_INVOKABLE void setSelToode(int id);

signals:
    void cListChanged();
    void selectedToodeChanged();
    void getDataChanged();
    void alamIndexChanged();
private:
    void setCList(DVtableList * plist);
    DVtableList *cList() const;
    DVtableList *mCtList;
    QString m_selectedToode;
    int m_alamIndex = 0;
    int tooteid = 0;
    QVector<QString> columnTitles = {"detaili nr","detail" ,"juurde + 5%" ,"HETKEL" ,"tehtud" , "KUHU", "tehtud"};
};

#endif // TOOTEDTABLEVIEWMODEL_H
