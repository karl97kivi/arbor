#ifndef USERDATA_H
#define USERDATA_H

#include "server.h"

#include <QObject>




class UserData : public QObject
{
    Q_OBJECT
public:
    explicit UserData(QObject *parent = nullptr);


    QString m_user;



signals:

public slots:
    void setUser(QString user);


    QString getUser();

};

#endif // USERDATA_H
