#ifndef WORKBENCHVIEW_H
#define WORKBENCHVIEW_H

#include <QNetworkAccessManager>
#include <QObject>
#include <QVector>
#include "wblistmodel.h"



class workbenchview : public QObject
{
    Q_OBJECT

    Q_PROPERTY(bool confirmButton READ confirmButton WRITE setConfirmButton NOTIFY confirmButtonChanged)
    Q_PROPERTY(QString selectedBench READ selectedBench WRITE setSelectedBench NOTIFY selectedBenchChanged)
public:
    explicit workbenchview(QObject *parent = nullptr);


    bool m_confirmButton;
    bool confirmButton();

    QString m_selectedBench;
    QString selectedBench();


signals:
    void confirmButtonChanged();
    void selectedBenchChanged();





private:
    void setConfirmButton(bool buttonClicked);
    void setSelectedBench(QString selected);

};

#endif // WORKBENCHVIEW_H
