#ifndef WORKBENCHDATA_H
#define WORKBENCHDATA_H

#include "server.h"

#include <QObject>


struct Workbench {
    QString name;
    QString id;
};

class WorkbenchData : public QObject
{
    Q_OBJECT
public:
    explicit WorkbenchData(QObject *parent = nullptr);
    Workbench m_selectedBench;
    Q_INVOKABLE void setActiveBench(QString id_worker);
    Server server;
signals:

public slots:
    void setSelectedBenchName(QString bench);
    void setSelectedBenchId(QString benchid);

    QString getBenchName();
    QString getBenchId();
};

#endif // WORKBENCHDATA_H
