#include "list_entry.h"
#include <QEventLoop>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QNetworkReply>
#include <QUrlQuery>

EntryList::EntryList(QObject *parent) : QObject(parent)
{
    if (server.checkInternetConnection() == "OK") {
        QByteArray bytes = server.dataGET("getAndroidWorkerEntriesForDetail", "/2/59/21");
        QString str = QString::fromUtf8(bytes.data(), bytes.size());

        QJsonDocument document = QJsonDocument::fromJson(bytes);

        QJsonArray array = document.array();
        qDebug() << "fordetail:" << str << document;

        foreach (const QJsonValue &v, array) {
            EntryItems newItem;
            newItem.kellaaeg = v.toObject().value("kellaeg").toString();
            newItem.kuupaev = v.toObject().value("kuupaev").toString();
            newItem.lisatud_kogus = v.toObject().value("amount").toInt();
            appendItem(newItem);
        }

    } else if (server.checkInternetConnection() == "NOT OK") {
        qDebug() << "No internet connection (productList)";
    }
}



QVector<EntryItems> EntryList::entries() const
{
    return mEntryList;
}


void EntryList::refreshList(QString workerid, QString benchid, QString detailid)
{
    qDebug() << "refreshList entries: " << workerid << benchid << detailid;
    if (server.checkInternetConnection() == "OK") {

        mEntryList.clear();
        QString suburl = workerid + "/" + detailid + "/" + benchid ;
        QByteArray bytes = server.dataGET("getAndroidWorkerEntriesForDetail/", suburl);
        QString str = QString::fromUtf8(bytes.data(), bytes.size());
        QJsonDocument document = QJsonDocument::fromJson(bytes);
        QJsonArray array = document.array();

        foreach (const QJsonValue &v, array) {
            EntryItems newItem;
            newItem.kellaaeg = v.toObject().value("kellaeg").toString();
            newItem.kuupaev = v.toObject().value("kuupaev").toString();
            newItem.lisatud_kogus = v.toObject().value("amount").toInt();
            appendItem(newItem);
        }

    } else if (server.checkInternetConnection() == "NOT OK") {
        qDebug() << "No internet connection (productList)";
    }
}

void EntryList::addEntry(QString workerid, QString benchid, QString detailid, QString lisatud)
{
    QJsonObject jsondata1;
    jsondata1.insert("id_detail", detailid);
    jsondata1.insert("id_toopink", benchid);
    jsondata1.insert("id_tootaja", workerid);
    jsondata1.insert("lisatud", lisatud);

    QByteArray jsonbyte1 = QJsonDocument(jsondata1).toJson(QJsonDocument::JsonFormat::Compact);

    QUrlQuery query1;
    query1.addQueryItem(QString("tehtud").toUtf8(), QString(jsonbyte1).toUtf8() );
    if (server.checkInternetConnection() == "OK") {
        server.dataPOST("addTehtudSissekanne", query1);
        QByteArray bytes = server.dataGET("download/", "DA-85.png");
        QString str = QString::fromUtf8(bytes.data(), bytes.size());
        qDebug() << "image data: "<< str;
    }


}

void EntryList::appendItem(EntryItems entry)
{
    mEntryList.append(entry);
}
