#ifndef WORKHOURMODEL_H
#define WORKHOURMODEL_H

#include <QAbstractTableModel>

class WorkhourModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    explicit WorkhourModel(QObject *parent = nullptr);

    enum TableRoles {
        TableDataRole = Qt::UserRole +1,
        HeadingRole,
        TitleRole,
        WidthRole
    };

    // Header:
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    virtual QHash<int, QByteArray> roleNames() const override;

private:
    QVector<QString> columnTitles = {"Tööpink", "kuupäev",  "Toode", "detail", "Kogus", "Hind"};
};

#endif // WORKHOURMODEL_H
