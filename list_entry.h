#ifndef LIST_SISSEKANNE_H
#define LIST_SISSEKANNE_H

#include <QObject>
#include <QVector>
#include "server.h"


struct EntryItems {

    int lisatud_kogus;
    QString kellaaeg;
    QString kuupaev;

};

struct AddEntry {
    int id;
    int toopink_id;
    int detail_id;
    int lisatud_kogus;
    QString kuupaev;

};



class EntryList : public QObject
{
    Q_OBJECT
public:
    explicit EntryList(QObject *parent = nullptr);
    QVector<EntryItems> entries() const;
    Server server;
public slots:
    void appendItem(EntryItems sissekanne);
    Q_INVOKABLE void refreshList(QString workerid, QString benchid, QString detailid);
    Q_INVOKABLE void addEntry(QString workerid, QString benchid, QString detailid, QString lisatud);

private:
    QVector<EntryItems> mEntryList;


};

#endif // LIST_SISSEKANNE_H
