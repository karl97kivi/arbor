import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.12
import QtQuick.Controls.Styles 1.4
import app.login 1.0
 Rectangle {
    id: loginRect

    //anchors.fill: parent
    width: 800
    anchors.horizontalCenter: parent.horizontalCenter
    anchors.top: parent.top
    anchors.topMargin: 75
    anchors.bottom: parent.bottom
    anchors.bottomMargin: 75
    Loader {
        id: loader

    }

   LoginHandler {
    id: loginview
    onCredentialStatusUnSuccessful: {
        console.log("credentials incorrect")
        incorrectlogin.text = "Logimine ebaõnnestus. Proovi uuesti"
    }

    onCredentialStatusChanged: {
     //console.log("finally here, tubli karl");
        //loginRect.visible = false
        //loader.source = "Workbench.qml"
        workbenchdata.setSelectedBenchId(0)
        workbenchdata.setActiveBench(activedata.getUserKood())
        userdata.setUser(loginview.user())
        activedata.setUserKood(pintextfield.text)
        activedata.setUser_id(loginview.userid())

        incorrectlogin.text = ""
        loginid.visible = false;
        workbenchid.visible = true;
        workbenchid.focus = true;
    }
    onNoInternetConnection: {
        console.log("No internet connection detected by QML");
        incorrectlogin.text = "Interneti ühendus puudub. Võimalik, et Wi-Fi ei tööta."
        //internetconnection_id.visible = true;
    }
   }



    Rectangle {

        border.color: "#0097e6"
        border.width: 2
        radius: 10

        anchors.fill: parent



        ColumnLayout {
            spacing: 5

            anchors.fill: parent



            RowLayout {
                id: pinLayout
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottom: buttonLayout.top
                anchors.bottomMargin: 50
                signal editingFinished()

                Rectangle {
                    width: 70
                    anchors.right: pintextfield.left
                    Label {
                        anchors.centerIn: parent
                        text: "Kood:"
                        font.pixelSize: 25
                    }
                }

                TextField {
                    id: pintextfield
                    padding: 8
                    anchors.fill: parent
                    //activeFocusOnPress: false
                    background: Rectangle {
                        radius: 5
                        border.color: "#C9D9E2"
                        implicitWidth: 250
                        implicitHeight: 40
                        border.width: 1
                    }
                    text: ""
                    font.pixelSize: 20


                }





            }

            RowLayout {

                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottom: pinLayout.top
                anchors.bottomMargin: 20
                Text {
                    id: incorrectlogin
                    font.pixelSize: 15
                    text: ""
                    color: "red"

                }
            }
            RowLayout {
                id: buttonLayout
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottom: gridLayout.top
                anchors.bottomMargin: 50
                Button {
                     id: button1
                    text: "Logi sisse"
                    font.pixelSize: 20

                    onClicked: {
                        //console.log(pintextfield.text)
                        loginview.credentialStatus = true
                        activedata.setUserKood(pintextfield.text)
                        pintextfield.text = ""
                        loginview.setPin(pintextfield.text)


                    }

                    background: Rectangle {
                        implicitHeight: 70
                        implicitWidth: 300
                        border.width: 1
                        color: button1.down ? "#B2DFF7" : "#0097e6"
                        border.color: "#C9D9E2"
                        radius: 5
                    }

                }
            }
            RowLayout {
                id: gridLayout
                anchors.bottom:parent.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottomMargin: 10
                GridView {
                    id: grid

                    width: 750
                    height: 300

                    clip: true
                    cellWidth: 150; cellHeight: 100
                    focus: true


                    model: ListModel {
                        ListElement { name: "A" }
                        ListElement { name: "B" }
                        ListElement { name: "C" }
                        ListElement { name: "D" }
                        ListElement { name: "Tühjenda" }
                        ListElement { name: "1" }
                        ListElement { name: "2" }
                        ListElement { name: "3" }
                        ListElement { name: "4" }
                        ListElement { name: "5" }
                        ListElement { name: "6" }
                        ListElement { name: "7" }
                        ListElement { name: "8" }
                        ListElement { name: "9" }
                        ListElement { name: "0" }
                    }



                    delegate: Component {
                         id: contactsDelegate
                         Rectangle {
                             id: wrapper
                             width: 145
                             height: 95
                             border.color: "#C9D9E2"
                             border.width: 3
                             color: "white"//GridView.isCurrentItem ? "#0097e6" : "white"
                             radius: 5
                             Text {
                                 id: toopinktextfield
                                 anchors { centerIn: parent }
                                 text: name
                                 font.pixelSize: 25
                                 color: "black"//wrapper.GridView.isCurrentItem ? "white" : "black"

                             }
                             MouseArea {
                                 property int count: 0
                                 onPressedChanged: {
                                     if (count == 1) {
                                         toopinktextfield.color = "black"
                                         wrapper.color = "white"
                                         count = 0;
                                     } else {
                                         wrapper.color = "#0097e6"
                                         toopinktextfield.color = "white"
                                         count++;
                                     }
                                 }

                                 anchors.fill: parent
                                 onClicked: {
                                     //console.log("item", index, name)
                                     parent.GridView.view.currentIndex = index
                                     if (name == "Tühjenda") {
                                         pintextfield.text = ""
                                         loginview.setPin("")
                                     } else {

                                         pintextfield.text = pintextfield.text + name
                                         loginview.setPin(pintextfield.text)
                                         activedata.setUserKood(pintextfield.text)
                                     }


                                 }

                             }
                         }

                     }
                }
            }

        }


    }

}

