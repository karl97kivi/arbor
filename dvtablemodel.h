#ifndef DVTABLEMODEL_H
#define DVTABLEMODEL_H

#include <QAbstractTableModel>
#include "dvtablelist.h"

class DVTableModel : public QAbstractTableModel
{
    Q_OBJECT
    Q_PROPERTY(DVtableList *cList READ cList WRITE setCList NOTIFY cListChanged)
    Q_PROPERTY(QString selectedToode READ selectedToode WRITE setSelectedToode NOTIFY selectedToodeChanged)

public:
    explicit DVTableModel(QObject *parent = nullptr);
    virtual ~DVTableModel() override;

    enum TableRoles {
        TableDataRole = Qt::UserRole +1,
        HeadingRole,
        TitleRole
    };

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    virtual QHash<int, QByteArray> roleNames() const override;

    Q_INVOKABLE QString getData(int row, QString par);

    void setSelectedToode(QString);
    QString selectedToode();
    Q_INVOKABLE void setInitToode();


signals:
    void cListChanged();
    void selectedToodeChanged();
    void getDataChanged();
private:
    void setCList(DVtableList * plist);
    DVtableList *cList() const;
    DVtableList *mCtList;
    bool first_at;
    QVector<QString> columnTitles = {"detaili nr", "detail", "pkilp", "must mõõt", "puhas mõõt", "tk", "juurde + 5%", "tehtud"};
    const QVector<int> m_headerRows;
    QString m_selectedToode;
    int tooteid = 0;
};

#endif // DVTABLEMODEL_H
