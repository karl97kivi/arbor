#include "tableview_workhour.h"

WorkhourModel::WorkhourModel(QObject *parent)
    : QAbstractTableModel(parent)
{
}

QVariant WorkhourModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    // FIXME: Implement me!
    return "lahe";
}

int WorkhourModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return 20;
}

int WorkhourModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return 6;
}

QVariant WorkhourModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    switch(role) {
    case TableDataRole:

        return "loc " + QString::number(index.column()) + "-" + QString::number(index.row());


    case TitleRole:
        return 18;
    case WidthRole:
        if (index.column() == 0) {
            return 200;
        }
        if (index.column() == 1) {
            return 200;
        }
        if (index.column() == 2) {
            return 160;
        }
        if (index.column() == 3) {
            return 200;
        }
        if (index.column() == 4) {
            return 100;
        }
        if (index.column() == 5) {
            return 100;
        } else {
            return 100;
        }
    default:
        break;
    }

    // FIXME: Implement me!
    return QVariant("dsdadas");
}


QHash<int, QByteArray> WorkhourModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[TableDataRole] = "tabledata";
    roles[HeadingRole] = "heading";
    roles[TitleRole] = "title";
    roles[WidthRole] = "widthrole";
    return roles;
}
