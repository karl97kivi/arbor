#include "wblistmodel.h"

#include "wblist.h"
#include <QObject>

workbenchListModel::workbenchListModel(QObject *parent)
    : QAbstractListModel(parent),
      mList(nullptr)
{

}

int workbenchListModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid() || !mList)
        return 0;

    // FIXME: Implement me!
    return mList->items().size();
}

QVariant workbenchListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() || !mList)
        return QVariant();

    const gridItems item = mList->items().at(index.row());
    switch(role) {
    case IdRole:
        return QVariant(item.id);
    case NameRole:
        return QVariant(item.name);
    }
    return QVariant();
}

QHash<int, QByteArray> workbenchListModel::roleNames() const
{
    QHash<int, QByteArray> names;
    names[IdRole] = "id";
    names[NameRole] = "name";
    return names;
}

WBList *workbenchListModel::list() const
{
    //qDebug() << "list is read";
    return mList;
}

void workbenchListModel::setList(WBList *list)
{
    if (mList) {
        mList->disconnect(this);
    }

    //qDebug() << "data is set in QML" << list->items().last().name;

    beginResetModel();
    endResetModel();
     mList = list;

    emit dataChanged(createIndex(0,0), createIndex(20,20));
    //emit listChanged();
}

void workbenchListModel::testing()
{
    //qDebug() << "testing func called";
    emit listChanged();
}
