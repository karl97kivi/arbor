#ifndef LOGINVIEW_H
#define LOGINVIEW_H

#include <QString>
#include <QObject>
#include <QStringListModel>
#include <QNetworkReply>
#include <QJsonObject>
#include <QUrlQuery>
#include "server.h"



class LoginView : public QObject
{
    Q_OBJECT


    //Q_PROPERTY(QString pin  WRITE setPin)
    Q_PROPERTY(bool credentialStatus WRITE setCredentialStatus NOTIFY credentialStatusChanged)

public:
    explicit LoginView(QObject *parent = nullptr);
    QString m_user;
    QString m_userid;
    QString m_pin;
    bool m_credentialStatus;


    QNetworkAccessManager *manager;
    QNetworkReply *reply;



    void setUser(const QString user);





public slots:
    void replyFinished(QNetworkReply*reply1);
    void setPin(QString pin);
    QString userid() const;
    void setUserid(const QString &userid);
    QString pin();
    QString user();

signals:
    void credentialStatusChanged();
    void credentialStatusUnSuccessful();
    void noInternetConnection();

private:


    Server server;

    void validateCredentials();
    bool setCredentialStatus(bool status);
    //bool credentialStatus();
};




#endif // LOGINVIEW_H
