import QtQuick 2.12
import QtQuick.Controls 1.4

Item {
    anchors.fill: parent

    Rectangle {
        ListModel {
              id: libraryModel
              ListElement {
                  title: "A Masterpiece"
                  author: "Gabriel"
              }
              ListElement {
                  title: "Brilliance"
                  author: "Jens"
              }
              ListElement {
                  title: "Outstanding"
                  author: "Frederik"
              }
          }
        anchors.fill : parent
        Text {
            text: "alamtootenimiasdasdasdasdasdasdsas"
        }
        TableView {

            TableViewColumn { role: "title"; title: "Title"; width: 100 }
            TableViewColumn { role: "author"; title: "Author"; width: 200 }
            model: libraryModel
        }
    }
}
