#include "server.h"

#include <QEventLoop>
#include <QJsonObject>
#include <QJsonDocument>
#include <QUrlQuery>

Server::Server(QObject *parent) : QObject(parent)
{

}

QByteArray Server::dataGET(QString subUrl, QString data)
{
    manager = new QNetworkAccessManager();

    QString url_string = QString(SERVER_URL) + subUrl + data;
    QUrl serviceUrl = QUrl(url_string);
    QNetworkRequest request(serviceUrl);

    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded;charset=utf-8;");
    request.setRawHeader("Content-Type", "application/x-www-form-urlencoded;charset=utf-8;");

    reply =  manager->get(request);
    QEventLoop event;
    connect(manager, SIGNAL(finished(QNetworkReply*)), &event, SLOT(quit()));
    event.exec();

    QByteArray bytes = reply->readAll();
    return bytes;
}

QByteArray Server::dataPOST(QString subUrl, QUrlQuery query)
{
    manager = new QNetworkAccessManager();

    QByteArray postData;


    QUrl params;
    params.setQuery(query);

    postData = params.toEncoded(QUrl::RemoveFragment);


    QString url_string = QString(SERVER_URL) + subUrl;
    QUrl serviceUrl = QUrl(url_string);
    QNetworkRequest request(serviceUrl);

    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded;charset=utf-8;");
    request.setRawHeader("Content-Type", "application/x-www-form-urlencoded;charset=utf-8;");

    reply = manager->post(request, postData);

    QEventLoop event;
    connect(manager, SIGNAL(finished(QNetworkReply*)), &event, SLOT(quit()));
    event.exec();

    QByteArray bytes = reply->readAll();
    return bytes;
}

QString Server::checkInternetConnection()
{
    QByteArray bytes = dataGET("checkConnectivity", "");
    QString str = QString::fromUtf8(bytes.data(), bytes.size());
    if (str == "CONNECTION_OK") return "OK";
    else return "NOT OK";
}

