#ifndef LISTTABLEMODEL_H
#define LISTTABLEMODEL_H

#include <QAbstractListModel>
#include "dvtablelist.h"

class ListTableModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(DVtableList *tooted READ tooted WRITE setTooted NOTIFY tootedChanged)
    Q_PROPERTY(int count READ getCount WRITE setCount NOTIFY countChanged)
    Q_PROPERTY(int rownum READ getRownum WRITE setRownum NOTIFY rownumChanged)

public:
    explicit ListTableModel(QObject *parent = nullptr);

    // Header:
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole) override;
    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    enum TableRoles {
        NameRole,
        TkRole

    };
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    // Remove data:
    bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;
    virtual QHash<int, QByteArray> roleNames() const override;
    Q_INVOKABLE void deleteRows();

signals:
    void tootedChanged();
    void countChanged();
    void rownumChanged();

private:
    void setRownum(int rownum);
    int getRownum();
    void setCount(int val);
    int getCount();
    int m_count;
    int last_count;
    int m_rownum;

    void setTooted(DVtableList *list);
    DVtableList *tooted() const;
    DVtableList *m_tooted;
};

#endif // LISTTABLEMODEL_H
