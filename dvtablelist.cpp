#include "dvtablelist.h"

#include <QEventLoop>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QEventLoop>
#include "server.h"

DVtableList::DVtableList(QObject *parent) : QObject(parent)
{

    /*manager = new QNetworkAccessManager();
    qDebug() << "str1";
    QUrl serviceUrl = QUrl(QString(SERVER_URL)+"getAllBenchDetails/1");
    QNetworkRequest request(serviceUrl);

    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded;charset=utf-8;");
    request.setRawHeader("Content-Type", "application/x-www-form-urlencoded;charset=utf-8;");



    reply =  manager->get(request);
    QEventLoop event;
    connect(manager, SIGNAL(finished(QNetworkReply*)), &event, SLOT(quit()));
    QEventLoop::ProcessEventsFlags flags = QFlags<QEventLoop::ProcessEventsFlag>();
    event.exec(flags);


    QByteArray bytes = reply->readAll();
    QString str = QString::fromUtf8(bytes.data(), bytes.size());
    qDebug() << "Read bytes from getallBench: " << str;*/
    //qDebug() << "toopingid: " << str;

    if (server.checkInternetConnection() == "OK") {
        qDebug() << "There is internet connection";
        QByteArray bytes = server.dataGET("getAllBenchDetails", "/1");
        //QByteArray bytes = reply1->readAll();
        QString str = QString::fromUtf8(bytes.data(), bytes.size());
        qDebug() << "str value: " << str;
        if (str != "EMPTY" && str != "") {
            QJsonDocument document = QJsonDocument::fromJson(bytes);

            QJsonArray tooted = document.array();
            QJsonValue alamtoode = tooted.first();
        //    qDebug() << "testingalamtoode: " << alamtoode.toObject().value("alamtooted").toArray().first().toObject().value("detailid").toArray().first().toObject().value("dnr");

            // alamtooted iteration
            //QJsonArray alamTooted = alamtoode.toObject().value("alamtooted").toArray();
            Q_ASSERT(tooted.size() > 0);
            foreach (const QJsonValue &toode, tooted) {
                Toode newToode;
                newToode.tootenimi = toode.toObject().value("tootenimi").toString();
                newToode.toote_id = QString::number(toode.toObject().value("id_toode").toInt());
                newToode.rownr = toode.toObject().value("jarjekorranr").toInt();
                //qDebug() << toode.toObject().value("id_toode").toInt() << " peaks töötama";
                //newToode.alamtooted = toode.toObject().toVariantMap()["alamtooted"].toList();


        //        qDebug() << "toode: " << toode.toObject().value("tootenimi");
                QVector<Alamtoode> alamtemp;
                QJsonArray alamTooted = toode.toObject().value("alamtooted").toArray();
                foreach (const QJsonValue &val, alamTooted) {
                    Alamtoode newAlamtoode;
                    newAlamtoode.alamtootenimi = val.toObject().value("alamtootenimi").toString();
                    newAlamtoode.alamtootekogus = val.toObject().value("alamtootekogus").toInt();
                    //newAlamtoode.detailid = val.toObject().toVariantMap()["detailid"].toList();


        //            qDebug() << "--->alamtoode: " << val.toObject().value("alamtootenimi");
                    QJsonArray detailid = val.toObject().value("detailid").toArray();

                    foreach (const QJsonValue &det, detailid) {
                        Detail newDetail;
                        newDetail.id = det.toObject().value("id_detail").toInt();
                        newDetail.dnr = det.toObject().value("dnr").toString();
                        newDetail.detail = det.toObject().value("nimi").toString();
                        newDetail.kilp = det.toObject().value("pkilp").toString();
                        newDetail.puhas = det.toObject().value("pmoot").toString();
                        newDetail.must = det.toObject().value("mmoot").toString();
                        newDetail.tk = det.toObject().value("tk").toInt();
                        newDetail.pv = det.toObject().value("pv").toInt();
                        newDetail.tehtud = det.toObject().value("tehtud").toInt();
                        newDetail.lisa_juurde = det.toObject().value("lisa_juurde").toInt();
                        newDetail.hetkel = det.toObject().value("hetkel").toString();
                        qDebug() << "dvlist detail Hetkel: " << det.toObject().value("hetkel").toInt();
                        newDetail.toopink = det.toObject().value("toopink").toString();
                        newDetail.kuhu = det.toObject().value("kuhu_name").toString();
        //                qDebug() << "------> detail: " << det.toObject().value("nimi");
                        newAlamtoode.detailid.append(newDetail);
                    }
                    alamtemp.append(newAlamtoode);
                }
                newToode.alamtooted = alamtemp;

                appendList(newToode);
            }
        } else {
            /* if there is no töös tooteid, then this is fired. HANDLE THIS*/
        }

    } else if (server.checkInternetConnection() == "NOT OK") {
        qDebug() << "No internet connection (DVTablelist)";
    }


}



QVector<Toode> DVtableList::listTooted()
{
    return tooted;
}



void DVtableList::refreshTooteData(QString ite)
{
    tooted.clear();
    qDebug() << "refreshTooteData"<< ite;
    if (server.checkInternetConnection() == "OK") {
        QByteArray bytes = server.dataGET("getAllBenchDetails", "/" + getBenchid(ite));
        //QByteArray bytes = reply1->readAll();
        QString str = QString::fromUtf8(bytes.data(), bytes.size());
        qDebug() << getBenchid(ite) << "str value: " << str;
        if (str != "EMPTY") {
            QJsonDocument document = QJsonDocument::fromJson(bytes);
            //qDebug() << "document: " << document;
            QJsonArray tooted = document.array();
            QJsonValue alamtoode = tooted.first();
        //    qDebug() << "testingalamtoode: " << alamtoode.toObject().value("alamtooted").toArray().first().toObject().value("detailid").toArray().first().toObject().value("dnr");

            // alamtooted iteration
            //QJsonArray alamTooted = alamtoode.toObject().value("alamtooted").toArray();
            //Q_ASSERT(tooted.size() > 0);
            foreach (const QJsonValue &toode, tooted) {
                Toode newToode;
                newToode.tootenimi = toode.toObject().value("tootenimi").toString();
                newToode.toote_id = QString::number(toode.toObject().value("id_toode").toInt());
                newToode.rownr = toode.toObject().value("jarjekorranr").toInt();
                //newToode.alamtooted = toode.toObject().toVariantMap()["alamtooted"].toList();


        //       qDebug() << "toode: " << toode.toObject().value("tootenimi");
                QVector<Alamtoode> alamtemp;
                QJsonArray alamTooted = toode.toObject().value("alamtooted").toArray();
                foreach (const QJsonValue &val, alamTooted) {
                    Alamtoode newAlamtoode;
                    newAlamtoode.alamtootenimi = val.toObject().value("alamtootenimi").toString();
                    newAlamtoode.alamtootekogus = val.toObject().value("alamtootekogus").toInt();
                    //newAlamtoode.detailid = val.toObject().toVariantMap()["detailid"].toList();


        //            qDebug() << "--->alamtoode: " << val.toObject().value("alamtootenimi");
                    QJsonArray detailid = val.toObject().value("detailid").toArray();

                    foreach (const QJsonValue &det, detailid) {
                        Detail newDetail;
                        newDetail.id = det.toObject().value("id_detail").toInt();
                        newDetail.dnr = det.toObject().value("dnr").toString();
                        newDetail.detail = det.toObject().value("nimi").toString();
                        newDetail.kilp = det.toObject().value("pkilp").toString();
                        newDetail.puhas = det.toObject().value("pmoot").toString();
                        newDetail.must = det.toObject().value("mmoot").toString();
                        newDetail.tk = det.toObject().value("tk").toInt();
                        newDetail.pv = det.toObject().value("pv").toInt();
                        newDetail.tehtud = det.toObject().value("tehtud").toInt();
                        newDetail.lisa_juurde = det.toObject().value("lisa_juurde").toInt();
                        newDetail.hetkel = det.toObject().value("hetkel").toString();
                        newDetail.toopink = det.toObject().value("toopink").toString();
                        newDetail.kuhu = det.toObject().value("kuhu_name").toString();
        //                qDebug() << "------> detail: " << det.toObject().value("nimi");
                        newAlamtoode.detailid.append(newDetail);
                    }
                    alamtemp.append(newAlamtoode);
                }
                newToode.alamtooted = alamtemp;

                appendList(newToode);
            }
        }
    } else if (server.checkInternetConnection() == "NOT OK") {
        qDebug() << "No internet connection (refreshTooteData)";
    }

}

QString DVtableList::getFirstTooteID()
{
    if (tooted.size() > 0) {
        return tooted[0].toote_id;
    } else {
        return "0";
    }
}

QString DVtableList::getBenchid(QString ite)
{
    if (server.checkInternetConnection() == "OK") {
        QByteArray bytes = server.dataGET("getAllBenches", "");
        QString str = QString::fromUtf8(bytes.data(), bytes.size());
        //qDebug() << "toopingid: " << str;
        if (str != "EMPTY") {
            QJsonDocument document = QJsonDocument::fromJson(bytes);
           // qDebug() << "document: " << document;
            QJsonArray array = document.array();
           // qDebug() << "array: " << array;

            foreach (const QJsonValue &v, array) {
                QString name = v.toObject().value("NIMI").toString();
             //   qDebug() << "name: " << name << "ite: " << ite;
                if (name == ite) {

                    int s = v.toObject().value("ID").toInt();
              //       qDebug() << "finally here, id: " << QString::number(s);
                    return QString::number(s);
                }
                //qDebug() << v.toObject().value("ID").toInt();
                int id = v.toObject().value("ID").toInt();

                //qDebug() << v.toObject().value("NIMI").toString();

            }

        }
        return "0";
    } else if (server.checkInternetConnection() == "NOT OK") {
        qDebug() << "No internet connection (getBenchid)";
    }
    return "0";
}

void DVtableList::appendList(Toode item)
{
    tooted.append(item);
}
