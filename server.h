#ifndef SERVER_H
#define SERVER_H

#include <QNetworkReply>
#include <QObject>

#define SERVER_URL "http://192.168.1.200/"
//#define SERVER_URL "http://192.168.43.124/"

class Server : public QObject
{
    Q_OBJECT
public:
    explicit Server(QObject *parent = nullptr);
    QNetworkReply *reply;
    QNetworkAccessManager * manager;

    QByteArray dataGET(QString subUrl, QString data);
    QByteArray dataPOST(QString subUrl, QUrlQuery query);
    QString checkInternetConnection();



signals:
    void noInternetConnection();

public slots:
};

#endif // SERVER_H
