#include "sissekanne_model.h"

sissekanne_model::sissekanne_model(QObject *parent)
    : QAbstractListModel(parent)
{
}

QVariant sissekanne_model::headerData(int section, Qt::Orientation orientation, int role) const
{
    // FIXME: Implement me!
    return "";
}

int sissekanne_model::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid())
        return 0;

    // FIXME: Implement me!
     return 10;//m_sissekanded->sissekanded().size();
}

QVariant sissekanne_model::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    // FIXME: Implement me!
    return QVariant("String");
}

void sissekanne_model::setSissekanded(EntryList *sissekanded_list)
{
    if (sissekanded_list) {
        sissekanded_list->disconnect(this);
    }

    //qDebug() << "data is set in QML" << list->items().last().name;

    beginResetModel();
    endResetModel();
    m_sissekanded = sissekanded_list;

    emit dataChanged(createIndex(0,0), createIndex(20,0));

}
