#ifndef DETAILDATA_H
#define DETAILDATA_H

#include <QObject>
#include <QUrlQuery>
#include <QEventLoop>
#include <QJsonDocument>
#include <QJsonObject>
#include "server.h"



class DetailData : public QObject
{
    Q_OBJECT
public:
    explicit DetailData(QObject *parent = nullptr);
    QString m_clickedDetail;

    QString m_kogus;
    int m_tehtud;
    int m_lisatud;
    QString m_detailid;
    Q_INVOKABLE QString updateData(QString userid, QString detailid, QString bench, QString kogus);
    Q_INVOKABLE QString findImages(QString detail_dnr);



    Server server;
signals:
    void detailDataStructChanged();
public slots:

    void setClickedDetail(QString detail);

    void setKogus(QString kogus);
    void setTehtud(int tehtud);
    void setLisatud(int lisatud);
    void setTehtudToZero();
    void setLisatudToZero();
    void setDetailid(QString data);
    QString getClickedDetail();

    QString getKogus();
    int getTehtud();
    int getLisatud();
    QString getDetailid();


};

#endif // DETAILDATA_H
