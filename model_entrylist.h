#ifndef ENTRYLISTMODEL_H
#define ENTRYLISTMODEL_H

#include <QAbstractListModel>


class EntryList;
class EntryListModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(EntryList *list READ list WRITE setList NOTIFY listChanged)

public:
    explicit EntryListModel(QObject *parent = nullptr);
    enum {
        DateRole = Qt::UserRole,
        TimeRole,
        AmountRole
    };

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    virtual QHash<int, QByteArray> roleNames() const override;
    EntryList *list() const;
    void setList(EntryList *list);

private:
    EntryList * mList;

signals:
    void listChanged();
};

#endif // ENTRYLISTMODEL_H
