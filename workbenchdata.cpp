#include "workbenchdata.h"


WorkbenchData::WorkbenchData(QObject *parent) : QObject(parent)
{

}

void WorkbenchData::setActiveBench(QString id_worker)
{
    if (server.checkInternetConnection() == "OK") {
        QByteArray bytes = server.dataGET("setActiveBench", "/" + id_worker + "/" + m_selectedBench.id);
        //QByteArray bytes = reply1->readAll();
        QString str = QString::fromUtf8(bytes.data(), bytes.size());
        qDebug() << str << "nonii active bench set?"<<m_selectedBench.id<<id_worker;
    }else if (server.checkInternetConnection() == "NOT OK") {
        qDebug() << "No internet connection (setActiveBench)";
    }
}

QString WorkbenchData::getBenchName()
{
    return m_selectedBench.name;
}

QString WorkbenchData::getBenchId()
{
    return m_selectedBench.id;
}

void WorkbenchData::setSelectedBenchName(QString benchname)
{
    m_selectedBench.name = benchname;
}

void WorkbenchData::setSelectedBenchId(QString benchid)
{
    m_selectedBench.id = benchid;
}
