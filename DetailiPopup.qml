import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
//import app.model.tableview 0.1
import QtGraphicalEffects 1.12

import app.EntryList 1.0
//import app.popup 1.0
Rectangle {
    id: d_id
    anchors.fill: parent

    property string red: "#F6CDE6"
    property string orange: "#FFE5BC"
    property string green: "#DDF9D9"
    property string blue: "#ADD9FE"

    property string addNumber: "0"
    onActiveFocusChanged: {
        if (focus == true)  {
            console.log("popup active")


            //detailinimid.text = "Detaili NR: " + detaildata.getClickedDetail();
            kogusid.text = "Kogus: " + detaildata.getKogus();
            tehtudid.text = "Tehtud: " + detaildata.getTehtud();

            entryList.refreshList(activedata.getUser_id() ,workbenchdata.getBenchId(),detaildata.getDetailid());
            entrylistid.list = entryList;
            if (activedata.getSelectedToodeID() === "0") {
                activedata.setSelectedToodeID(Clist.getFirstTooteID());
                console.log("selected was0")
            }

            kommentaartext.text = DDStruct.getComment(activedata.getSelectedToodeID())
            function getColor(){


                var strl = (detaildata.getKogus()).split(' ');
                //console.log(parseInt(strl[0]) + parseInt(strl[2]), " kogus1")
                if (detaildata.getTehtud() <= 0 ) return red;
                if (detaildata.getTehtud() > 0 && detaildata.getTehtud() < strl[0]) return orange;
                if (detaildata.getTehtud() >= strl[0] && detaildata.getTehtud() < (parseInt(strl[0]) + parseInt(strl[2]))) return green;
                if (detaildata.getTehtud() >= (parseInt(strl[0]) + parseInt(strl[2]))) return blue;
            }
            reckogus.color = getColor()
            rectehtud.color = getColor()
        }
        if (focus == false) {
            //console.log("inactive focus popup")
            timerid.stop()
        }
    }


    Timer {
        id: timerid
        interval: 0500; running: false; repeat: false
        onTriggered: detaildata.updateData(activedata.getUser_id(), detaildata.getDetailid(), workbenchdata.getBenchName(), detaildata.getTehtud())
    }


    Rectangle {
       id: topmenubar
       anchors.top: parent.top
       anchors.right: parent.right
       anchors.left: parent.left
       height: 100
       Rectangle {
           anchors.right: parent.right
           anchors.left: parent.left
           height: 100
           gradient: Gradient {
               GradientStop { position: 0.0; color: "#0097e6" }
               GradientStop { position: 1.0; color: "#B2DFF7" }
           }
           Button {
               id: userid
               anchors.verticalCenter: parent.verticalCenter
               anchors.left: parent.left
               anchors.leftMargin: 24
               font.pixelSize: 24
               text: "Joonised"
               background: Rectangle {

                   radius: 5
               }
               MouseArea {
                   anchors.fill: parent

                   onClicked: {
                       detailpopupid.visible = false
                       detailpopupid.focus = false
                       imageview_id.focus = true
                       imageview_id.visible = true
                       detaildata.findImages("jou");
                   }
               }

           }




           Button {
               id: logoutButton
               anchors.verticalCenter: parent.verticalCenter
               anchors.right: parent.right
               anchors.rightMargin: 24
               text: "Tagasi"

               font.pixelSize: 24
               background: Rectangle {
                   implicitHeight: 40
                   implicitWidth: 100
                   border.width: 1
                   color: button.down ? "#d6d6d6" : "#f6f6f6"
                   border.color: "#C9D9E2"
                   radius: 5
               }
               MouseArea {
                   anchors.fill: parent

                   onClicked: {
                       //activedata.setSelectedToodeID("0");

                       detailpopupid.visible = false
                       detailpopupid.focus = false
                       dataviewid.focus = true
                       dataviewid.visible = true
                       kommentaartext.text = ""
                       // frompopup = false

                       detaildata.updateData(activedata.getUser_id(),detaildata.getDetailid(), workbenchdata.getBenchName(), detaildata.getTehtud())

                   }
               }
           }


       }


    }
    Rectangle {
        id: dataviewTab
        anchors.top: topmenubar.bottom
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        Rectangle {
            id: informationtab


            anchors.bottom: parent.bottom
            anchors.top: parent.top
            anchors.left: parent.left
            width: 400
            color: "darkturquoise"
            ColumnLayout {
                id: infoColumnTab
                property var nameratio: 0.55 // percent in width
                property var valueratio: 0.45 // percent in width
                property var borderwidth: 1
                property var borderheight: 2
                property var fontsize_name: 18
                property var fontsize_value: 15
                spacing: 0
                anchors.fill: informationtab
                Rectangle {
                    id: info_1
                    anchors.fill:parent
                    ColumnLayout  {
                        Rectangle {
                            width: informationtab.width
                            height: 50
                            Rectangle {
                                id: itemname
                                width: parent.width * infoColumnTab.nameratio
                                height: parent.height
                                anchors.left: parent.left
                                Text {
                                    anchors.left: parent.left
                                    anchors.leftMargin: 24
                                    anchors.verticalCenter: parent.verticalCenter
                                    width: parent.width - 5
                                    text: "Detaili nr:"
                                    font.pixelSize: infoColumnTab.fontsize_name
                                    wrapMode: Text.WordWrap
                                }
                            }
                            Rectangle {
                                width: infoColumnTab.borderwidth
                                anchors.verticalCenter: parent.verticalCenter
                                anchors.right: itemname.right
                                height: parent.height - 5
                                color: "#e0e0e0"

                            }
                            Rectangle {
                                id: itemvalue
                                width: parent.width * infoColumnTab.valueratio
                                height: parent.height
                                anchors.right: parent.right
                                Text {
                                    anchors.left: parent.left
                                    anchors.leftMargin: 24
                                    width: parent.width - 5
                                    anchors.verticalCenter: parent.verticalCenter
                                    text: "ITS-244dda sdasdas sdasd"
                                    font.pixelSize: infoColumnTab.fontsize_value
                                    wrapMode: Text.WordWrap
                                }
                            }

                            Rectangle {
                                height: infoColumnTab.borderheight
                                anchors.horizontalCenter: parent.horizontalCenter
                                anchors.bottom: parent.bottom
                                width: informationtab.width - 5
                                color: "#e0e0e0"

                            }
                        }
                        Rectangle {
                            width: informationtab.width
                            height: 50
                            Rectangle {
                                id: itemname_1
                                width: parent.width * infoColumnTab.nameratio
                                height: parent.height
                                anchors.left: parent.left
                                Text {
                                    anchors.left: parent.left
                                    anchors.leftMargin: 24
                                    anchors.verticalCenter: parent.verticalCenter
                                    width: parent.width - 5
                                    text: "Nimetus:"
                                    font.pixelSize: infoColumnTab.fontsize_name
                                    wrapMode: Text.WordWrap
                                }
                            }
                            Rectangle {
                                width: infoColumnTab.borderwidth
                                anchors.verticalCenter: parent.verticalCenter
                                anchors.right: itemname_1.right
                                height: parent.height - 5
                                color: "#e0e0e0"

                            }
                            Rectangle {
                                width: parent.width * infoColumnTab.valueratio
                                height: parent.height
                                anchors.right: parent.right
                                Text {
                                    anchors.left: parent.left
                                    anchors.leftMargin: 24
                                    width: parent.width - 5
                                    anchors.verticalCenter: parent.verticalCenter
                                    text: "Laua plate pikk põõn"
                                    font.pixelSize: infoColumnTab.fontsize_value
                                    wrapMode: Text.WordWrap
                                }
                            }

                            Rectangle {
                                height: infoColumnTab.borderheight
                                anchors.horizontalCenter: parent.horizontalCenter
                                anchors.bottom: parent.bottom
                                width: informationtab.width - 5
                                color: "#e0e0e0"

                            }
                        }
                        Rectangle {
                            width: informationtab.width
                            height: 50
                            Rectangle {
                                id: itemname_2
                                width: parent.width * infoColumnTab.nameratio
                                height: parent.height
                                anchors.left: parent.left
                                Text {
                                    anchors.left: parent.left
                                    anchors.leftMargin: 24
                                    anchors.verticalCenter: parent.verticalCenter
                                    width: parent.width - 5
                                    text: "p.kilp"
                                    font.pixelSize: infoColumnTab.fontsize_name
                                    wrapMode: Text.WordWrap
                                }
                            }
                            Rectangle {
                                width: infoColumnTab.borderwidth
                                anchors.verticalCenter: parent.verticalCenter
                                anchors.right: itemname_2.right
                                height: parent.height - 5
                                color: "#e0e0e0"

                            }
                            Rectangle {
                                width: parent.width * infoColumnTab.valueratio
                                height: parent.height
                                anchors.right: parent.right
                                Text {
                                    anchors.left: parent.left
                                    anchors.leftMargin: 24
                                    width: parent.width - 5
                                    anchors.verticalCenter: parent.verticalCenter
                                    text: "2013x596x20"
                                    font.pixelSize: infoColumnTab.fontsize_value
                                    wrapMode: Text.WordWrap
                                }
                            }

                            Rectangle {
                                height: infoColumnTab.borderheight
                                anchors.horizontalCenter: parent.horizontalCenter
                                anchors.bottom: parent.bottom
                                width: informationtab.width - 5
                                color: "#e0e0e0"

                            }
                        }
                        Rectangle {
                            width: informationtab.width
                            height: 50
                            Rectangle {
                                id: itemname_3
                                width: parent.width * infoColumnTab.nameratio
                                height: parent.height
                                anchors.left: parent.left
                                Text {
                                    anchors.left: parent.left
                                    anchors.leftMargin: 24
                                    anchors.verticalCenter: parent.verticalCenter
                                    width: parent.width - 5
                                    text: "Must Mõõt"
                                    font.pixelSize: infoColumnTab.fontsize_name
                                    wrapMode: Text.WordWrap
                                }
                            }
                            Rectangle {
                                width: infoColumnTab.borderwidth
                                anchors.verticalCenter: parent.verticalCenter
                                anchors.right: itemname_3.right
                                height: parent.height - 5
                                color: "#e0e0e0"

                            }
                            Rectangle {
                                width: parent.width * infoColumnTab.valueratio
                                height: parent.height
                                anchors.right: parent.right
                                Text {
                                    anchors.left: parent.left
                                    anchors.leftMargin: 24
                                    width: parent.width - 5
                                    anchors.verticalCenter: parent.verticalCenter
                                    text: "2043x622,5x21,5"
                                    font.pixelSize: infoColumnTab.fontsize_value
                                    wrapMode: Text.WordWrap
                                }
                            }

                            Rectangle {
                                height: infoColumnTab.borderheight
                                anchors.horizontalCenter: parent.horizontalCenter
                                anchors.bottom: parent.bottom
                                width: informationtab.width - 5
                                color: "#e0e0e0"

                            }
                        }
                        Rectangle {
                            width: informationtab.width
                            height: 50
                            Rectangle {
                                id: itemname_4
                                width: parent.width * infoColumnTab.nameratio
                                height: parent.height
                                anchors.left: parent.left
                                Text {
                                    anchors.left: parent.left
                                    anchors.leftMargin: 24
                                    anchors.verticalCenter: parent.verticalCenter
                                    width: parent.width - 5
                                    text: "Puhas Mõõt"
                                    font.pixelSize: infoColumnTab.fontsize_name
                                    wrapMode: Text.WordWrap
                                }
                            }
                            Rectangle {
                                width: infoColumnTab.borderwidth
                                anchors.verticalCenter: parent.verticalCenter
                                anchors.right: itemname_4.right
                                height: parent.height - 5
                                color: "#e0e0e0"

                            }
                            Rectangle {
                                width: parent.width * infoColumnTab.valueratio
                                height: parent.height
                                anchors.right: parent.right
                                Text {
                                    anchors.left: parent.left
                                    anchors.leftMargin: 24
                                    width: parent.width - 5
                                    anchors.verticalCenter: parent.verticalCenter
                                    text: "2013x596x20"
                                    font.pixelSize: infoColumnTab.fontsize_value
                                    wrapMode: Text.WordWrap
                                }
                            }

                            Rectangle {
                                height: infoColumnTab.borderheight
                                anchors.horizontalCenter: parent.horizontalCenter
                                anchors.bottom: parent.bottom
                                width: informationtab.width - 5
                                color: "#e0e0e0"

                            }
                        }
                        Rectangle {
                            width: informationtab.width
                            height: 50
                            Rectangle {
                                id: itemname_5
                                width: parent.width * infoColumnTab.nameratio
                                height: parent.height
                                anchors.left: parent.left
                                Text {
                                    anchors.left: parent.left
                                    anchors.leftMargin: 24
                                    anchors.verticalCenter: parent.verticalCenter
                                    width: parent.width - 5
                                    text: "Tüki arv"
                                    font.pixelSize: infoColumnTab.fontsize_name
                                    wrapMode: Text.WordWrap
                                }
                            }
                            Rectangle {
                                width: infoColumnTab.borderwidth
                                anchors.verticalCenter: parent.verticalCenter
                                anchors.right: itemname_5.right
                                height: parent.height - 5
                                color: "#e0e0e0"

                            }
                            Rectangle {
                                width: parent.width * infoColumnTab.valueratio
                                height: parent.height
                                anchors.right: parent.right
                                Text {
                                    anchors.left: parent.left
                                    anchors.leftMargin: 24
                                    width: parent.width - 5
                                    anchors.verticalCenter: parent.verticalCenter
                                    text: "12"
                                    font.pixelSize: infoColumnTab.fontsize_value
                                    wrapMode: Text.WordWrap
                                }
                            }

                            Rectangle {
                                height: infoColumnTab.borderheight
                                anchors.horizontalCenter: parent.horizontalCenter
                                anchors.bottom: parent.bottom
                                width: informationtab.width - 5
                                color: "#e0e0e0"

                            }
                        }
                        Rectangle {
                            width: informationtab.width
                            height: 50
                            Rectangle {
                                id: itemname_6
                                width: parent.width * infoColumnTab.nameratio
                                height: parent.height
                                anchors.left: parent.left
                                Text {
                                    anchors.left: parent.left
                                    anchors.leftMargin: 24
                                    anchors.verticalCenter: parent.verticalCenter
                                    width: parent.width - 5
                                    text: "Kilbi arv"
                                    font.pixelSize: infoColumnTab.fontsize_name
                                    wrapMode: Text.WordWrap
                                }
                            }
                            Rectangle {
                                width: infoColumnTab.borderwidth
                                anchors.verticalCenter: parent.verticalCenter
                                anchors.right: itemname_6.right
                                height: parent.height - 5
                                color: "#e0e0e0"

                            }
                            Rectangle {
                                width: parent.width * infoColumnTab.valueratio
                                height: parent.height
                                anchors.right: parent.right
                                Text {
                                    anchors.left: parent.left
                                    anchors.leftMargin: 24
                                    width: parent.width - 5
                                    anchors.verticalCenter: parent.verticalCenter
                                    text: "158"
                                    font.pixelSize: infoColumnTab.fontsize_value
                                    wrapMode: Text.WordWrap
                                }
                            }

                            Rectangle {
                                height: infoColumnTab.borderheight
                                anchors.horizontalCenter: parent.horizontalCenter
                                anchors.bottom: parent.bottom
                                width: informationtab.width - 5
                                color: "#e0e0e0"

                            }
                        }
                        Rectangle {
                            width: informationtab.width
                            height: 50
                            Rectangle {
                                id: itemname_7
                                width: parent.width * infoColumnTab.nameratio
                                height: parent.height
                                anchors.left: parent.left
                                Text {
                                    anchors.left: parent.left
                                    anchors.leftMargin: 24
                                    anchors.verticalCenter: parent.verticalCenter
                                    width: parent.width - 5
                                    text: "Ühest kilbist mitu detaili k.det"
                                    font.pixelSize: infoColumnTab.fontsize_name
                                    wrapMode: Text.WordWrap
                                }
                            }
                            Rectangle {
                                width: infoColumnTab.borderwidth
                                anchors.verticalCenter: parent.verticalCenter
                                anchors.right: itemname_7.right
                                height: parent.height - 5
                                color: "#e0e0e0"

                            }
                            Rectangle {
                                width: parent.width * infoColumnTab.valueratio
                                height: parent.height
                                anchors.right: parent.right
                                Text {
                                    anchors.left: parent.left
                                    anchors.leftMargin: 24
                                    width: parent.width - 5
                                    anchors.verticalCenter: parent.verticalCenter
                                    text: "1"
                                    font.pixelSize: infoColumnTab.fontsize_value
                                    wrapMode: Text.WordWrap
                                }
                            }

                            Rectangle {
                                height: infoColumnTab.borderheight
                                anchors.horizontalCenter: parent.horizontalCenter
                                anchors.bottom: parent.bottom
                                width: informationtab.width - 5
                                color: "#e0e0e0"

                            }
                        }
                        Rectangle {
                            width: informationtab.width
                            height: 50
                            Rectangle {
                                id: itemname_8
                                width: parent.width * infoColumnTab.nameratio
                                height: parent.height
                                anchors.left: parent.left
                                Text {
                                    anchors.left: parent.left
                                    anchors.leftMargin: 24
                                    anchors.verticalCenter: parent.verticalCenter
                                    width: parent.width - 5
                                    text: "Mitu ribi kokku R"
                                    font.pixelSize: infoColumnTab.fontsize_name
                                    wrapMode: Text.WordWrap
                                }
                            }
                            Rectangle {
                                width: infoColumnTab.borderwidth
                                anchors.verticalCenter: parent.verticalCenter
                                anchors.right: itemname_8.right
                                height: parent.height - 5
                                color: "#e0e0e0"

                            }
                            Rectangle {
                                width: parent.width * infoColumnTab.valueratio
                                height: parent.height
                                anchors.right: parent.right
                                Text {
                                    anchors.left: parent.left
                                    anchors.leftMargin: 24
                                    width: parent.width - 5
                                    anchors.verticalCenter: parent.verticalCenter
                                    text: "15R"
                                    font.pixelSize: infoColumnTab.fontsize_value
                                    wrapMode: Text.WordWrap
                                }
                            }

                            Rectangle {
                                height: infoColumnTab.borderheight
                                anchors.horizontalCenter: parent.horizontalCenter
                                anchors.bottom: parent.bottom
                                width: informationtab.width - 5
                                color: "#e0e0e0"

                            }
                        }
                        Rectangle {
                            width: informationtab.width
                            height: 50
                            Rectangle {
                                id: itemname_9
                                width: parent.width * infoColumnTab.nameratio
                                height: parent.height
                                anchors.left: parent.left
                                Text {
                                    anchors.left: parent.left
                                    anchors.leftMargin: 24
                                    anchors.verticalCenter: parent.verticalCenter
                                    width: parent.width - 5
                                    text: "TÜKI HIND"
                                    font.pixelSize: infoColumnTab.fontsize_name
                                    wrapMode: Text.WordWrap
                                }
                            }
                            Rectangle {
                                width: infoColumnTab.borderwidth
                                anchors.verticalCenter: parent.verticalCenter
                                anchors.right: itemname_9.right
                                height: parent.height - 5
                                color: "#e0e0e0"

                            }
                            Rectangle {
                                width: parent.width * infoColumnTab.valueratio
                                height: parent.height
                                anchors.right: parent.right
                                Text {
                                    anchors.left: parent.left
                                    anchors.leftMargin: 24
                                    width: parent.width - 5
                                    anchors.verticalCenter: parent.verticalCenter
                                    text: "0.15"
                                    font.pixelSize: infoColumnTab.fontsize_value
                                    wrapMode: Text.WordWrap
                                }
                            }

                            Rectangle {
                                height: infoColumnTab.borderheight
                                anchors.horizontalCenter: parent.horizontalCenter
                                anchors.bottom: parent.bottom
                                width: informationtab.width - 5
                                color: "#e0e0e0"

                            }
                        }
                    }


                    color: "white"
                }
            }
        }
        Rectangle {
            id: workbenchEntriesTab
            anchors.bottom: parent.bottom
            anchors.top: parent.top
            anchors.left: informationtab.right
            width: 200

            Rectangle {
                id: title_box_id
                anchors.top: parent.top
                width: parent.width
                height:50
                Text {

                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    font.pixelSize: 25
                    text: "Sissekanded"
                }
                Rectangle {
                    anchors.bottom: parent.bottom
                    anchors.horizontalCenter: parent.horizontalCenter
                    width: parent.width -32 //20
                    height: 2
                    color: "#0097e6"
                }

            }





            ListView {
                id: sissekanded_list_id
                anchors.top: title_box_id.bottom
                width: parent.width
                height: 500

                ListModel {
                      id: libraryModel
                      ListElement {
                          title: "26.05.20 Kell 18:12 "
                          author: "Lisatud: 150"
                      }
                      ListElement {
                          title: "26.05.20 Kell 18:12 "
                          author: "Lisatud: 120"
                      }
                      ListElement {
                          title: "27.05.20 Kell 10:10"
                          author: "Lisatud: 1020"
                      }
                      ListElement {
                          title: "27.05.20 Kell 10:10"
                          author: "Lisatud: 1020"
                      }
                      ListElement {
                          title: "27.05.20 Kell 10:10"
                          author: "Lisatud: 1020"
                      }
                  }
                //anchors.bottom: parent.bottom
                model: libraryModel /*EntryListModel {
                    id: entrylistid
                    list: entryList
                }*/
                 clip: true
                delegate: Rectangle {
                    width: parent.width
                    anchors.horizontalCenter: parent.horizontalCenter
                    height: 45
                    Text {
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.left: parent.left
                        anchors.leftMargin: 16
                        text: model.kuupaev + " Kell: " + model.kellaaeg + "\n"  +"Lisatud:" + model.kogus
                        font.pixelSize: 15
                    }
                    Rectangle {
                        anchors.bottom: parent.bottom
                        //anchors.left: parent.left
                        //anchors.leftMargin: 16
                        anchors.horizontalCenter: parent.horizontalCenter
                        width: parent.width -32 //20
                        height: 1
                        color: "#D3D3D3" //"#0097e6"
                    }
                }
            }

            Rectangle {
                anchors.bottom: parent.bottom
                anchors.left: parent.left
                anchors.top: parent.top
                width: 2
                color: "#0097e6" //"#D3D3D3"
            }
            Rectangle {
                anchors.bottom: parent.bottom
                anchors.right: parent.right
                anchors.top: parent.top
                width: 2
                color: "#0097e6" //"#D3D3D3"
            }

        }
        Rectangle {
            id: addEntriesTab
            anchors.bottom: parent.bottom
            anchors.top: parent.top
            anchors.right: parent.right
            anchors.left: workbenchEntriesTab.right

            //color: "darkgoldenrod"
            Rectangle {
                id: commentTab
                anchors.top: addEntriesTab.top
                width: addEntriesTab.width
                height:70
                Text {
                    anchors.centerIn: parent
                    text: "Vesiliim on vaja siin"
                    color: "firebrick"
                    font.pixelSize: 35
                }
                MouseArea {
                    anchors.fill: parent

                    onClicked: {
                        console.log("Comment clicked, make some window appear to show all comment text!")
                    }
                }
            }

            Rectangle {
                id: labelsTab
                anchors.top: commentTab.bottom
                width: addEntriesTab.width
                height: 80
                //color: "darkslateblue"


                Rectangle {
                    id: amountTab
                    anchors.right: labelsTab.right
                    width: parent.width/2
                    height:parent.height
                    //color: "darkkhaki"
                    Text {
                        anchors.centerIn: parent
                        text: "Tehtud: 2090"
                        font.pixelSize: 35
                    }
                }
                Rectangle {
                    id: doneTab
                    anchors.left: labelsTab.left
                    width: parent.width/2
                    height:parent.height
                    //color: "darkorange"
                    Text {
                        anchors.centerIn: parent
                        text: "Kogus: 2017 + 101"
                        font.pixelSize: 35
                    }
                }
                Rectangle {
                    anchors.bottom: parent.bottom
                    anchors.horizontalCenter: parent.horizontalCenter
                    width: parent.width - 10
                    height: 2
                    color: "#D3D3D3" //"#0097e6"
                }


            }
            Rectangle {
                id: numberButtonsTab
                anchors.top: labelsTab.bottom
                anchors.bottom: addEntriesTab.bottom
                width: parent.width

                //color: "darksalmon"
                Rectangle {
                    id: textfieldTab
                    height: 80
                    width: numberButtonsTab.width
                    anchors.top: numberButtonsTab.top
                    //color: "firebrick"

                    Text {
                        id: labelNumbers
                        anchors.centerIn: parent
                        text: addNumber
                        font.pixelSize: 35
                    }
                    Rectangle {
                        anchors.bottom: parent.bottom
                        anchors.horizontalCenter: parent.horizontalCenter
                        width: parent.width - 10
                        height: 2
                        color: "#D3D3D3" //"#0097e6"
                    }
                }
                Rectangle {
                    id: buttonsTab
                    width: numberButtonsTab.width
                    anchors.top: textfieldTab.bottom
                    anchors.bottom: numberButtonsTab.bottom
                    //color: "darkviolet"
                    GridView {
                        id: grid
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.horizontalCenterOffset: 20
                        anchors.verticalCenterOffset: 20
                        anchors.verticalCenter: parent.verticalCenter
                        width: parent.width -35
                        height: parent.height

                        clip: true
                        cellWidth: (parent.width - 50)/3; cellHeight: (parent.height - 50)/4
                        focus: true


                        model: ListModel {
                            ListElement { name: "7" }
                            ListElement { name: "8" }
                            ListElement { name: "9" }
                            ListElement { name: "4" }
                            ListElement { name: "5" }
                            ListElement { name: "6" }
                            ListElement { name: "1" }
                            ListElement { name: "2" }
                            ListElement { name: "3" }
                            ListElement { name: "0" }
                            ListElement { name: "Tühjenda" }
                            ListElement { name: "Salvesta" }
                        }



                        delegate: Component {
                             id: contactsDelegate
                             Rectangle {
                                 id: wrapper
                                 width: (parent.width - 50)/3 -7
                                 height: (parent.height - 50)/4-7
                                 border.color: "#0097e6" //"#C9D9E2"

                                 color: "white"
                                 radius: 5
                                 Text {
                                     id: toopinktextfield
                                     anchors { centerIn: parent }
                                     text: name
                                     font.pixelSize: 35
                                     color: "black"
                                 }
                                 MouseArea {

                                     property int count: 0
                                     onPressedChanged: {
                                         if (count == 1) {
                                             toopinktextfield.color = "black"
                                             wrapper.color = "white"
                                             count = 0;
                                         } else {
                                             wrapper.color = "#0097e6"
                                             toopinktextfield.color = "white"
                                             count++;
                                         }
                                     }

                                     anchors.fill: parent
                                     onClicked: {
                                         if (index > -1 && index < 10) {
                                             console.log("add these together");
                                             if (addNumber.charAt(0) == '0') {
                                                 addNumber = '';
                                             }
                                             addNumber = addNumber + name;


                                         }
                                         if (index == 10) {
                                             console.log("remove digit");
                                             addNumber = "0"
                                         }
                                         if (index == 11) {
                                             console.log("save this shitty");
                                             entryList.addEntry(activedata.getUser_id() ,workbenchdata.getBenchId(),detaildata.getDetailid(),detaildata.getLisatud());
                                         }

                                         console.log("item", index, name)
                                         parent.GridView.view.currentIndex = index

                                         detaildata.setLisatud(name)


                                         add_text_id.text = "Lisa: "  + detaildata.getLisatud()

                                     }

                                 }
                             }
                        }
                    }
                }
            }



        }
    }
}
/*
    Rectangle {
        anchors.top: topmenubar.bottom
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        visible: false
        Rectangle {
            id: detaildataid
            anchors.top: parent.top
            width: parent.width
            //color: "yellow"
            height: 100


            Rectangle {

                anchors.fill: parent
                height: 100
                Rectangle {
                    id: column1

                    height: parent.height
                    width: parent.width/6
                    Rectangle {
                        id: forborder
                        anchors.right: parent.right
                        anchors.left: parent.left
                        height: parent.height /2

                        Text {

                            anchors.verticalCenterOffset: 5
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.verticalCenter: parent.verticalCenter
                            text: qsTr("Detaili nr")
                            font.pixelSize: 25

                        }
                        Rectangle {
                            anchors.bottom: forborder.bottom
                            anchors.left: forborder.left
                            anchors.leftMargin: 15
                            anchors.rightMargin: 15
                            anchors.right: forborder.right
                            height: 3
                            color: "#D3D3D3"
                        }

                    }
                    Rectangle {
                        anchors.top: forborder.bottom
                        anchors.right: parent.right
                        anchors.left: parent.left
                        height: parent.height /2

                        Text {
                            anchors.verticalCenterOffset: -12
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.verticalCenter: parent.verticalCenter
                            text: DDStruct.dnr
                            font.pixelSize: 20

                        }

                    }
                }
                Rectangle {
                    id: column2
                    anchors.left: column1.right
                    height: parent.height
                    width: parent.width/6
                    Rectangle {
                        id: forborder1
                        anchors.right: parent.right
                        anchors.left: parent.left
                        height: parent.height /2

                        Text {

                            anchors.verticalCenterOffset: 5
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.verticalCenter: parent.verticalCenter
                            text: qsTr("Nimetus")
                            font.pixelSize: 25

                        }
                        Rectangle {
                            anchors.bottom: forborder1.bottom
                            anchors.left: forborder1.left
                            anchors.leftMargin: 15
                            anchors.rightMargin: 15
                            anchors.right: forborder1.right
                            height: 3
                            color: "#D3D3D3"
                        }

                    }
                    Rectangle {
                        anchors.top: forborder1.bottom
                        anchors.right: parent.right
                        anchors.left: parent.left
                        height: parent.height /2

                        Text {
                            anchors.verticalCenterOffset: -12
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.verticalCenter: parent.verticalCenter
                            text: DDStruct.name
                            font.pixelSize: 20

                        }

                    }
                }
                Rectangle {
                    id: column3
                    anchors.left: column2.right
                    height: parent.height
                    width: parent.width/6
                    Rectangle {
                        id: forborder2
                        anchors.right: parent.right
                        anchors.left: parent.left
                        height: parent.height /2

                        Text {

                            anchors.verticalCenterOffset: 5
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.verticalCenter: parent.verticalCenter
                            text: qsTr("p.kilp")
                            font.pixelSize: 25

                        }
                        Rectangle {
                            anchors.bottom: forborder2.bottom
                            anchors.left: forborder2.left
                            anchors.leftMargin: 15
                            anchors.rightMargin: 15
                            anchors.right: forborder2.right
                            height: 3
                            color: "#D3D3D3"
                        }

                    }
                    Rectangle {
                        anchors.top: forborder2.bottom
                        anchors.right: parent.right
                        anchors.left: parent.left
                        height: parent.height /2

                        Text {
                            anchors.verticalCenterOffset: -12
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.verticalCenter: parent.verticalCenter
                            text: DDStruct.pkilp
                            font.pixelSize: 20

                        }

                    }
                }
                Rectangle {
                    id: column4
                    anchors.left: column3.right
                    height: parent.height
                    width: parent.width/6
                    Rectangle {
                        id: forborder3
                        anchors.right: parent.right
                        anchors.left: parent.left
                        height: parent.height /2

                        Text {

                            anchors.verticalCenterOffset: 5
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.verticalCenter: parent.verticalCenter
                            text: qsTr("Must mõõt")
                            font.pixelSize: 25

                        }
                        Rectangle {
                            anchors.bottom: forborder3.bottom
                            anchors.left: forborder3.left
                            anchors.leftMargin: 15
                            anchors.rightMargin: 15
                            anchors.right: forborder3.right
                            height: 3
                            color: "#D3D3D3"
                        }

                    }
                    Rectangle {
                        anchors.top: forborder3.bottom
                        anchors.right: parent.right
                        anchors.left: parent.left
                        height: parent.height /2

                        Text {
                            anchors.verticalCenterOffset: -12
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.verticalCenter: parent.verticalCenter
                            text: DDStruct.mmoot
                            font.pixelSize: 20

                        }

                    }
                }
                Rectangle {
                    id: column5
                    anchors.left: column4.right
                    height: parent.height
                    width: parent.width/6
                    Rectangle {
                        id: forborder4
                        anchors.right: parent.right
                        anchors.left: parent.left
                        height: parent.height /2

                        Text {

                            anchors.verticalCenterOffset: 5
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.verticalCenter: parent.verticalCenter
                            text: qsTr("Puhas mõõt")
                            font.pixelSize: 25

                        }
                        Rectangle {
                            anchors.bottom: forborder4.bottom
                            anchors.left: forborder4.left
                            anchors.leftMargin: 15
                            anchors.rightMargin: 15
                            anchors.right: forborder4.right
                            height: 3
                            color: "#D3D3D3"
                        }

                    }
                    Rectangle {
                        anchors.top: forborder4.bottom
                        anchors.right: parent.right
                        anchors.left: parent.left
                        height: parent.height /2

                        Text {
                            anchors.verticalCenterOffset: -12
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.verticalCenter: parent.verticalCenter
                            text: DDStruct.pmoot
                            font.pixelSize: 20

                        }

                    }
                }
                Rectangle {
                    id: column6
                    anchors.left: column5.right
                    height: parent.height
                    width: parent.width/6
                    Rectangle {
                        id: forborder5
                        anchors.right: parent.right
                        anchors.left: parent.left
                        height: parent.height /2

                        Text {

                            anchors.verticalCenterOffset: 5
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.verticalCenter: parent.verticalCenter
                            text: qsTr("tüki arv")
                            font.pixelSize: 25

                        }
                        Rectangle {
                            anchors.bottom: forborder5.bottom
                            anchors.left: forborder5.left
                            anchors.leftMargin: 15
                            anchors.rightMargin: 15
                            anchors.right: forborder5.right
                            height: 3
                            color: "#D3D3D3"
                        }

                    }
                    Rectangle {
                        anchors.top: forborder5.bottom
                        anchors.right: parent.right
                        anchors.left: parent.left
                        height: parent.height /2

                        Text {
                            anchors.verticalCenterOffset: -12
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.verticalCenter: parent.verticalCenter
                            text: DDStruct.tk
                            font.pixelSize: 20

                        }

                    }
                }






            }




        }


        Rectangle {
            id: kommetaarid
            anchors.top: detaildataid.bottom
            width: parent.width
            height: 75



            Text {
                id: kommentaartext
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                text: ""
                color: "red"

                font.pixelSize: 45

            }

            Rectangle {
                anchors.bottom: parent.bottom
                width: parent.width
                height: 3
                color: "#0097e6" //"#D3D3D3"
            }

        }


        Rectangle {
            id: lower_left_box_id
            //color:"red"
            anchors.top: kommetaarid.bottom
            anchors.left: parent.left
            anchors.bottom: parent.bottom
            width: parent.width*0.35
            Rectangle {

                id: kogustehtudid

                anchors.top: parent.top
                width: parent.width
                height: parent.height/2
                radius: 10
               Column {
                    anchors.fill: parent
                    function getColor(){


                        var strl = (detaildata.getKogus()).split(' ');
                        //console.log(parseInt(strl[0]) + parseInt(strl[2]), " kogus1")
                        if (detaildata.getTehtud() <= 0 ) return red;
                        if (detaildata.getTehtud() > 0 && detaildata.getTehtud() < strl[0]) return orange;
                        if (detaildata.getTehtud() >= strl[0] && detaildata.getTehtud() < (parseInt(strl[0]) + parseInt(strl[2]))) return green;
                        if (detaildata.getTehtud() >= (parseInt(strl[0]) + parseInt(strl[2]))) return blue;
                    }

                    Rectangle {
                        id: reckogus
                        height: parent.height/2
                        width: parent.width
                        color: getColor()


                        Text {
                            id: kogusid
                            //anchors.horizontalCenter: parent.horizontalCenter
                            anchors.verticalCenter: parent.verticalCenter
                            anchors.left: parent.left
                            anchors.leftMargin: 32
                            text: qsTr("Kogus: 1900 + 192")
                            font.pixelSize: 50

                        }


                    }

                    Rectangle {
                        id: rectehtud
                        height: parent.height/2
                        width: parent.width
                        color: getColor()
                        Text {
                            id: tehtudid
                            //anchors.horizontalCenter: parent.horizontalCenter
                            anchors.verticalCenter: parent.verticalCenter
                            anchors.left: parent.left
                            anchors.leftMargin: 32
                            text: "Tehtud: 0"


                            font.pixelSize: 50

                        }

                    }
                }



                Rectangle {
                    anchors.bottom: parent.bottom
                    anchors.left: parent.left
                    anchors.right: parent.right
                    height: 3
                    color: "#0097e6" //"#D3D3D3"
                }
            }

            Rectangle {
                id: saveandadd_id
                anchors.bottom: parent.bottom
                width: parent.width
                //anchors.bottom: amountgrid_id.top
                height: parent.height /2
                color: "white"

                Rectangle {
                    id: add_text_box_id
                    anchors.top: parent.top
                    height: parent.height/2
                    width: parent.width

                    color: "white"
                    Text {
                        id: add_text_id
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        text: qsTr("Lisa: 0")
                        font.pixelSize: 40

                    }
                }

                Rectangle {
                    id: save_button_id
                    anchors.top: add_text_box_id.bottom
                    height: parent.height/2
                    width: parent.width

                    color: "white"
                    Rectangle {
                        id: save_button_wrapper
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        width: 300
                        height: 75
                        border.color: "#0097e6"
                        border.width: 2
                        color: "white"
                        radius: 10
                        Text {
                            id: save_button_text_id
                            anchors { centerIn: parent }
                            text: "Salvesta"
                            font.pixelSize: 40
                            color: "black"
                        }
                        MouseArea {

                            property int count: 0
                            onPressedChanged: {
                                if (count == 1) {
                                    save_button_text_id.color = "black"
                                    save_button_wrapper.color = "white"
                                    count = 0;
                                } else {
                                    save_button_wrapper.color = "#0097e6"
                                    save_button_text_id.color = "white"
                                    count++;
                                }
                            }

                            anchors.fill: parent
                            onClicked: {
                                console.log("salvesta button clicked");

                                detaildata.setTehtud(detaildata.getLisatud())
                                entryList.addEntry(activedata.getUser_id() ,workbenchdata.getBenchId(),detaildata.getDetailid(),detaildata.getLisatud());
                                // clear the add value
                                detaildata.setLisatudToZero();
                                add_text_id.text = "Lisa: " + detaildata.getLisatud()

                                entryList.refreshList(activedata.getUser_id() ,workbenchdata.getBenchId(),detaildata.getDetailid());
                                entrylistid.list = entryList;
                                // change the color of the tehtud background
                                function getColor(){
                                    var strl = (detaildata.getKogus()).split(' ');
                                    //console.log(parseInt(strl[0]) + parseInt(strl[2]), " kogus1")
                                    if (detaildata.getTehtud() <= 0 ) return red;
                                    if (detaildata.getTehtud() > 0 && detaildata.getTehtud() < strl[0]) return orange;
                                    if (detaildata.getTehtud() >= strl[0] && detaildata.getTehtud() <= (parseInt(strl[0]) + parseInt(strl[2]))) return green;
                                    if (detaildata.getTehtud() > (parseInt(strl[0]) + parseInt(strl[2]))) return blue;
                                }
                                reckogus.color = getColor()
                                rectehtud.color = getColor()

                                tehtudid.text = "Tehtud: " + detaildata.getTehtud()

                               timerid.start()
                            }

                        }
                    }
                }








            }


        }
        Rectangle {
            id: lower_middle_box_id
            //color: "lime"
            anchors.top: kommetaarid.bottom
            anchors.left: lower_left_box_id.right
            anchors.bottom: parent.bottom
            width: parent.width*0.15

            ListModel {
                  id: libraryModel
                  ListElement {
                      title: "26.05.20 Kell 18:12 "
                      author: "Lisatud: 150"
                  }
                  ListElement {
                      title: "26.05.20 Kell 18:12 "
                      author: "Lisatud: 120"
                  }
                  ListElement {
                      title: "27.05.20 Kell 10:10"
                      author: "Lisatud: 1020"
                  }
              }
            Rectangle {
                id: title_box_id
                anchors.top: parent.top
                width: parent.width
                height:50
                Text {

                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    font.pixelSize: 25
                    text: "Sissekanded"
                }
                Rectangle {
                    anchors.bottom: parent.bottom
                    //anchors.left: parent.left
                    //anchors.leftMargin: 16
                    anchors.horizontalCenter: parent.horizontalCenter
                    width: parent.width -32 //20
                    height: 2
                    color: "#0097e6" //"#D3D3D3"
                }
            }

            ListView {
                id: sissekanded_list_id
                anchors.top: title_box_id.bottom
                width: parent.width
                anchors.bottom: parent.bottom
                model: EntryListModel {
                    id: entrylistid
                    list: entryList
                }
                 clip: true
                delegate: Rectangle {
                    width: parent.width
                    anchors.horizontalCenter: parent.horizontalCenter
                    height: 45
                    Text {
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.left: parent.left
                        anchors.leftMargin: 16
                        text: model.kuupaev + " Kell: " + model.kellaaeg + "\n"  +"Lisatud:" + model.kogus
                        font.pixelSize: 15
                    }
                    Rectangle {
                        anchors.bottom: parent.bottom
                        //anchors.left: parent.left
                        //anchors.leftMargin: 16
                        anchors.horizontalCenter: parent.horizontalCenter
                        width: parent.width -32 //20
                        height: 1
                        color: "#0097e6" //"#D3D3D3"
                    }
                }


            }

            Rectangle {
                anchors.bottom: parent.bottom
                anchors.left: parent.left
                anchors.top: parent.top
                width: 3
                color: "#0097e6" //"#D3D3D3"
            }
        }
        Rectangle {
            id: lower_right_box_id
            //color: "blue"
            anchors.top: kommetaarid.bottom
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            width: parent.width*0.5

            Rectangle {
                id: amountgrid_id
                anchors.fill: parent
                color: "white"

               // color: "lightgrey"



                GridView {
                    id: grid
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.horizontalCenterOffset: 20
                    anchors.verticalCenterOffset: 20
                    anchors.verticalCenter: parent.verticalCenter
                    width: parent.width -35
                    height: parent.height

                    clip: true
                    cellWidth: (parent.width - 50)/3; cellHeight: (parent.height - 50)/4
                    focus: true


                    model: ListModel {
                        ListElement { name: "1" }
                        ListElement { name: "5" }
                        ListElement { name: "10" }
                        ListElement { name: "25" }
                        ListElement { name: "50" }
                        ListElement { name: "100" }
                        ListElement { name: "-1" }
                        ListElement { name: "-5" }
                        ListElement { name: "-10" }
                        ListElement { name: "-25" }
                        ListElement { name: "-50" }
                        ListElement { name: "-100" }
                    }



                    delegate: Component {
                         id: contactsDelegate
                         Rectangle {
                             id: wrapper
                             width: (parent.width - 50)/3 -7
                             height: (parent.height - 50)/4-7
                             border.color: "#0097e6" //"#C9D9E2"

                             color: "white"//GridView.isCurrentItem ? "#0097e6" : "white"
                             radius: 5
                             Text {
                                 id: toopinktextfield
                                 anchors { centerIn: parent }
                                 text: name
                                 font.pixelSize: 35
                                 color: "black"//wrapper.GridView.isCurrentItem ? "white" : "black"
                             }
                             MouseArea {

                                 property int count: 0
                                 onPressedChanged: {
                                     if (count == 1) {
                                         toopinktextfield.color = "black"
                                         wrapper.color = "white"
                                         count = 0;
                                     } else {
                                         wrapper.color = "#0097e6"
                                         toopinktextfield.color = "white"
                                         count++;
                                     }
                                 }

                                 anchors.fill: parent
                                 onClicked: {
                                     //console.log("item", index, name)
                                     parent.GridView.view.currentIndex = index

                                     detaildata.setLisatud(name)


                                     add_text_id.text = "Lisa: "  + detaildata.getLisatud()

                                 }

                             }
                         }


                }



                }
            }

            Rectangle {
                anchors.bottom: parent.bottom
                anchors.left: parent.left
                anchors.top: parent.top
                width: 3
                color: "#0097e6" //"#D3D3D3"
            }

        }

    }




}
*/
