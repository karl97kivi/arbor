#ifndef POPUPWINDOW_H
#define POPUPWINDOW_H

#include <QObject>
#include "server.h"


struct DetailDataStruct {
    Q_GADGET
    QString m_dnr;
    QString m_name;
    QString m_pkilp;
    QString m_mmoot;
    QString m_pmoot;
    QString m_tk;
    Q_PROPERTY( QString dnr MEMBER m_dnr)
    Q_PROPERTY( QString name MEMBER m_dnr)
    Q_PROPERTY( QString pkilp MEMBER m_pkilp)
    Q_PROPERTY( QString mmoot MEMBER m_dnr)
    Q_PROPERTY( QString pmoot MEMBER m_dnr)
    Q_PROPERTY( QString tk MEMBER m_dnr)
};
//Q_DECLARE_METATYPE(DetailDataStruct)
class PopupWindow : public QObject
{
    Q_OBJECT

public:
    explicit PopupWindow(QObject *parent = nullptr);
/*
    DetailDataStruct dds;
    Q_PROPERTY( DetailDataStruct detailDataStruct READ getDetailDataStruct WRITE setDetailDataStruct NOTIFY detailDataStructChanged )

    void setDetailDataStruct(DetailDataStruct val);
    DetailDataStruct getDetailDataStruct();
*/


    QString m_dnr;
    QString m_name;
    QString m_pkilp;
    QString m_mmoot;
    QString m_pmoot;
    QString m_tk;
    QString m_comment;

    Q_PROPERTY( QString dnr READ getDnr WRITE setDnr NOTIFY dnrChanged)
    Q_PROPERTY( QString name READ getName WRITE setName NOTIFY nameChanged)
    Q_PROPERTY( QString pkilp READ getPkilp WRITE setPkilp NOTIFY pkilpChanged)
    Q_PROPERTY( QString mmoot READ getMmoot WRITE setMmoot NOTIFY mmootChanged)
    Q_PROPERTY( QString pmoot READ getPmoot WRITE setPmoot NOTIFY pmootChanged)
    Q_PROPERTY( QString tk READ getTk WRITE setTk NOTIFY tkChanged)


    QString getDnr();
    void setDnr(QString val);
    QString getName();
    void setName(QString val);
    QString getPkilp();
    void setPkilp(QString val);
    QString getMmoot();
    void setMmoot(QString val);
    QString getPmoot();
    void setPmoot(QString val);
    QString getTk();
    void setTk(QString val);

    Server server;
signals:
    //void detailDataStructChanged();
    void dnrChanged();
    void nameChanged();
    void pkilpChanged();
    void mmootChanged();
    void pmootChanged();
    void tkChanged();


public slots:
    QString getComment(QString id);
};


#endif // POPUPWINDOW_H
