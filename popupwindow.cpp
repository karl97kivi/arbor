#include "popupwindow.h"

#include <QDebug>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>



PopupWindow::PopupWindow(QObject *parent) : QObject(parent)
{

}

QString PopupWindow::getDnr()
{
    return m_dnr;
}

void PopupWindow::setDnr(QString val)
{
    m_dnr = val;
    emit dnrChanged();
}

QString PopupWindow::getName()
{
    return m_name;
}

void PopupWindow::setName(QString val)
{
    m_name = val;
    emit nameChanged();
}

QString PopupWindow::getPkilp()
{
    return m_pkilp;
}

void PopupWindow::setPkilp(QString val)
{
    m_pkilp = val;
    emit pkilpChanged();
}

QString PopupWindow::getMmoot()
{
    return m_mmoot;
}

void PopupWindow::setMmoot(QString val)
{
    m_mmoot = val;
    emit mmootChanged();
}

QString PopupWindow::getPmoot()
{
    return m_pmoot;
}

void PopupWindow::setPmoot(QString val)
{
    m_pmoot = val;
    emit pmootChanged();
}

QString PopupWindow::getTk()
{
    return m_tk;
}

void PopupWindow::setTk(QString val)
{
    m_tk = val;
    emit tkChanged();
}

QString PopupWindow::getComment(QString id)
{
    if (server.checkInternetConnection() == "OK") {
        QByteArray bytes = server.dataGET("getComment/", id);
        QString str = QString::fromUtf8(bytes.data(), bytes.size());

        QJsonDocument document = QJsonDocument::fromJson(bytes);

        QJsonArray array = document.array();
        QJsonValue val = array.first();

       // qDebug() << val.toObject()["kommentaar"].toString() << " kommentaar ja id " << id;
        return val.toObject()["kommentaar"].toString();
    }else if (server.checkInternetConnection() == "NOT OK") {
        qDebug() << "No internet connection (getComment)";
    }
    return "";
}





/*void PopupWindow::setDetailDataStruct(DetailDataStruct val)
{
    qDebug() << "setdetailds";
    dds = val;
    emit detailDataStructChanged();

}

DetailDataStruct PopupWindow::getDetailDataStruct()
{
    qDebug() << "getdetailds";
    return dds;
}*/
