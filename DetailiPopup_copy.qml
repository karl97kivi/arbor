import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
//import app.model.tableview 0.1
import QtGraphicalEffects 1.12
//import app.popup 1.0
Rectangle {
    id: d_id
    anchors.fill: parent


    property string red: "#F6CDE6"
    property string orange: "#FFE5BC"
    property string green: "#DDF9D9"
    property string blue: "#ADD9FE"
    onActiveFocusChanged: {
        if (focus == true)  {
            console.log("popup active")


            //detailinimid.text = "Detaili NR: " + detaildata.getClickedDetail();
            kogusid.text = "Kogus: " + detaildata.getKogus();
            tehtudid.text = "Tehtud: " + detaildata.getTehtud();
            if (activedata.getSelectedToodeID() === "0") {
                activedata.setSelectedToodeID(Clist.getFirstTooteID());
                console.log("selected was0")
            }

            kommentaartext.text = DDStruct.getComment(activedata.getSelectedToodeID())
            function getColor(){


                var strl = (detaildata.getKogus()).split(' ');
                //console.log(parseInt(strl[0]) + parseInt(strl[2]), " kogus1")
                if (detaildata.getTehtud() <= 0 ) return red;
                if (detaildata.getTehtud() > 0 && detaildata.getTehtud() < strl[0]) return orange;
                if (detaildata.getTehtud() >= strl[0] && detaildata.getTehtud() < (parseInt(strl[0]) + parseInt(strl[2]))) return green;
                if (detaildata.getTehtud() >= (parseInt(strl[0]) + parseInt(strl[2]))) return blue;
            }
            reckogus.color = getColor()
            rectehtud.color = getColor()
        }
        if (focus == false) {
            //console.log("inactive focus popup")
            timerid.stop()
        }
    }


    Timer {
        id: timerid
        interval: 0500; running: false; repeat: false
        onTriggered: detaildata.updateData(detaildata.getDetailid(), workbenchdata.getBenchName(), detaildata.getTehtud())
    }


    Rectangle {
       id: topmenubar
       anchors.top: parent.top
       anchors.right: parent.right
       anchors.left: parent.left
       height: 100
       Rectangle {
           anchors.right: parent.right
           anchors.left: parent.left
           height: 100
           gradient: Gradient {
               GradientStop { position: 0.0; color: "#0097e6" }
               GradientStop { position: 1.0; color: "#B2DFF7" }
           }
           Label {
               id: userid
               anchors.verticalCenter: parent.verticalCenter
               anchors.left: parent.left
               anchors.leftMargin: 24
               font.pixelSize: 24
               text: "Does it work?"
               color: "white"

           }




           Button {
               id: logoutButton
               anchors.verticalCenter: parent.verticalCenter
               anchors.right: parent.right
               anchors.rightMargin: 24
               text: "Tagasi"

               font.pixelSize: 24
               background: Rectangle {
                   implicitHeight: 40
                   implicitWidth: 100
                   border.width: 1
                   color: button.down ? "#d6d6d6" : "#f6f6f6"
                   border.color: "#C9D9E2"
                   radius: 5
               }
               MouseArea {
                   anchors.fill: parent

                   onClicked: {
                       //activedata.setSelectedToodeID("0");

                       detailpopupid.visible = false
                       detailpopupid.focus = false
                       dataviewid.focus = true
                       dataviewid.visible = true
                       kommentaartext.text = ""
                       // frompopup = false

                       detaildata.updateData(detaildata.getDetailid(), workbenchdata.getBenchName(), detaildata.getTehtud())

                   }
               }
           }


       }


    }
    Rectangle {
        anchors.top: topmenubar.bottom
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.bottom: parent.bottom

        Rectangle {
            id: detaildataid
            anchors.top: parent.top
            width: parent.width
            //color: "yellow"
            height: 100


            Rectangle {

                anchors.fill: parent
                height: 100
                Rectangle {
                    id: column1

                    height: parent.height
                    width: parent.width/6
                    Rectangle {
                        id: forborder
                        anchors.right: parent.right
                        anchors.left: parent.left
                        height: parent.height /2

                        Text {

                            anchors.verticalCenterOffset: 5
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.verticalCenter: parent.verticalCenter
                            text: qsTr("Detaili nr")
                            font.pixelSize: 25

                        }
                        Rectangle {
                            anchors.bottom: forborder.bottom
                            anchors.left: forborder.left
                            anchors.leftMargin: 15
                            anchors.rightMargin: 15
                            anchors.right: forborder.right
                            height: 3
                            color: "#D3D3D3"
                        }

                    }
                    Rectangle {
                        anchors.top: forborder.bottom
                        anchors.right: parent.right
                        anchors.left: parent.left
                        height: parent.height /2

                        Text {
                            anchors.verticalCenterOffset: -12
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.verticalCenter: parent.verticalCenter
                            text: DDStruct.dnr
                            font.pixelSize: 20

                        }

                    }
                }
                Rectangle {
                    id: column2
                    anchors.left: column1.right
                    height: parent.height
                    width: parent.width/6
                    Rectangle {
                        id: forborder1
                        anchors.right: parent.right
                        anchors.left: parent.left
                        height: parent.height /2

                        Text {

                            anchors.verticalCenterOffset: 5
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.verticalCenter: parent.verticalCenter
                            text: qsTr("Nimetus")
                            font.pixelSize: 25

                        }
                        Rectangle {
                            anchors.bottom: forborder1.bottom
                            anchors.left: forborder1.left
                            anchors.leftMargin: 15
                            anchors.rightMargin: 15
                            anchors.right: forborder1.right
                            height: 3
                            color: "#D3D3D3"
                        }

                    }
                    Rectangle {
                        anchors.top: forborder1.bottom
                        anchors.right: parent.right
                        anchors.left: parent.left
                        height: parent.height /2

                        Text {
                            anchors.verticalCenterOffset: -12
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.verticalCenter: parent.verticalCenter
                            text: DDStruct.name
                            font.pixelSize: 20

                        }

                    }
                }
                Rectangle {
                    id: column3
                    anchors.left: column2.right
                    height: parent.height
                    width: parent.width/6
                    Rectangle {
                        id: forborder2
                        anchors.right: parent.right
                        anchors.left: parent.left
                        height: parent.height /2

                        Text {

                            anchors.verticalCenterOffset: 5
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.verticalCenter: parent.verticalCenter
                            text: qsTr("p.kilp")
                            font.pixelSize: 25

                        }
                        Rectangle {
                            anchors.bottom: forborder2.bottom
                            anchors.left: forborder2.left
                            anchors.leftMargin: 15
                            anchors.rightMargin: 15
                            anchors.right: forborder2.right
                            height: 3
                            color: "#D3D3D3"
                        }

                    }
                    Rectangle {
                        anchors.top: forborder2.bottom
                        anchors.right: parent.right
                        anchors.left: parent.left
                        height: parent.height /2

                        Text {
                            anchors.verticalCenterOffset: -12
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.verticalCenter: parent.verticalCenter
                            text: DDStruct.pkilp
                            font.pixelSize: 20

                        }

                    }
                }
                Rectangle {
                    id: column4
                    anchors.left: column3.right
                    height: parent.height
                    width: parent.width/6
                    Rectangle {
                        id: forborder3
                        anchors.right: parent.right
                        anchors.left: parent.left
                        height: parent.height /2

                        Text {

                            anchors.verticalCenterOffset: 5
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.verticalCenter: parent.verticalCenter
                            text: qsTr("Must mõõt")
                            font.pixelSize: 25

                        }
                        Rectangle {
                            anchors.bottom: forborder3.bottom
                            anchors.left: forborder3.left
                            anchors.leftMargin: 15
                            anchors.rightMargin: 15
                            anchors.right: forborder3.right
                            height: 3
                            color: "#D3D3D3"
                        }

                    }
                    Rectangle {
                        anchors.top: forborder3.bottom
                        anchors.right: parent.right
                        anchors.left: parent.left
                        height: parent.height /2

                        Text {
                            anchors.verticalCenterOffset: -12
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.verticalCenter: parent.verticalCenter
                            text: DDStruct.mmoot
                            font.pixelSize: 20

                        }

                    }
                }
                Rectangle {
                    id: column5
                    anchors.left: column4.right
                    height: parent.height
                    width: parent.width/6
                    Rectangle {
                        id: forborder4
                        anchors.right: parent.right
                        anchors.left: parent.left
                        height: parent.height /2

                        Text {

                            anchors.verticalCenterOffset: 5
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.verticalCenter: parent.verticalCenter
                            text: qsTr("Puhas mõõt")
                            font.pixelSize: 25

                        }
                        Rectangle {
                            anchors.bottom: forborder4.bottom
                            anchors.left: forborder4.left
                            anchors.leftMargin: 15
                            anchors.rightMargin: 15
                            anchors.right: forborder4.right
                            height: 3
                            color: "#D3D3D3"
                        }

                    }
                    Rectangle {
                        anchors.top: forborder4.bottom
                        anchors.right: parent.right
                        anchors.left: parent.left
                        height: parent.height /2

                        Text {
                            anchors.verticalCenterOffset: -12
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.verticalCenter: parent.verticalCenter
                            text: DDStruct.pmoot
                            font.pixelSize: 20

                        }

                    }
                }
                Rectangle {
                    id: column6
                    anchors.left: column5.right
                    height: parent.height
                    width: parent.width/6
                    Rectangle {
                        id: forborder5
                        anchors.right: parent.right
                        anchors.left: parent.left
                        height: parent.height /2

                        Text {

                            anchors.verticalCenterOffset: 5
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.verticalCenter: parent.verticalCenter
                            text: qsTr("tüki arv")
                            font.pixelSize: 25

                        }
                        Rectangle {
                            anchors.bottom: forborder5.bottom
                            anchors.left: forborder5.left
                            anchors.leftMargin: 15
                            anchors.rightMargin: 15
                            anchors.right: forborder5.right
                            height: 3
                            color: "#D3D3D3"
                        }

                    }
                    Rectangle {
                        anchors.top: forborder5.bottom
                        anchors.right: parent.right
                        anchors.left: parent.left
                        height: parent.height /2

                        Text {
                            anchors.verticalCenterOffset: -12
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.verticalCenter: parent.verticalCenter
                            text: DDStruct.tk
                            font.pixelSize: 20

                        }

                    }
                }






            }




        }


        Rectangle {
            id: kommetaarid
            anchors.top: detaildataid.bottom
            width: parent.width
            height: 75



            Text {
                id: kommentaartext
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                text: ""
                color: "red"

                font.pixelSize: 35

            }

        }

        Rectangle {

            id: kogustehtudid

            anchors.top: kommetaarid.bottom
            width: parent.width
            height: 80
            radius: 10
            Row {
                anchors.fill: parent
                function getColor(){


                    var strl = (detaildata.getKogus()).split(' ');
                    //console.log(parseInt(strl[0]) + parseInt(strl[2]), " kogus1")
                    if (detaildata.getTehtud() <= 0 ) return red;
                    if (detaildata.getTehtud() > 0 && detaildata.getTehtud() < strl[0]) return orange;
                    if (detaildata.getTehtud() >= strl[0] && detaildata.getTehtud() < (parseInt(strl[0]) + parseInt(strl[2]))) return green;
                    if (detaildata.getTehtud() >= (parseInt(strl[0]) + parseInt(strl[2]))) return blue;
                }

                Rectangle {
                    id: reckogus
                    height: parent.height
                    width: parent.width/2
                    color: getColor()


                    Text {
                        id: kogusid
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        text: qsTr("Kogus: 1900 + 192")
                        font.pixelSize: 50

                    }


                }

                Rectangle {
                    id: rectehtud
                    height: parent.height
                    width: parent.width/2
                    color: getColor()
                    Text {
                        id: tehtudid
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        text: "Tehtud: 0"


                        font.pixelSize: 50

                    }

                }
            }



            Rectangle {
                anchors.bottom: parent.bottom
                anchors.left: parent.left
                anchors.right: parent.right
                height: 3
                color: "#D3D3D3"
            }
        }




        Rectangle {
            id: saveandadd_id
            anchors.top: kogustehtudid.bottom
            width: parent.width
            //anchors.bottom: amountgrid_id.top
            height: 100
            color: "white"


            Rectangle {
                anchors.left: parent.left
                height: parent.height
                width: parent.width/2

                color: "white"
                Rectangle {
                    id: save_button_wrapper
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    width: 300
                    height: 75
                    border.color: "#0097e6"
                    border.width: 2
                    color: "white"
                    radius: 10
                    Text {
                        id: save_button_text_id
                        anchors { centerIn: parent }
                        text: "Salvesta"
                        font.pixelSize: 40
                        color: "black"
                    }
                    MouseArea {

                        property int count: 0
                        onPressedChanged: {
                            if (count == 1) {
                                save_button_text_id.color = "black"
                                save_button_wrapper.color = "white"
                                count = 0;
                            } else {
                                save_button_wrapper.color = "#0097e6"
                                save_button_text_id.color = "white"
                                count++;
                            }
                        }

                        anchors.fill: parent
                        onClicked: {
                            console.log("salvesta button clicked");

                            detaildata.setTehtud(detaildata.getLisatud())
                            // clear the add value
                            detaildata.setLisatudToZero();
                            add_text_id.text = "Lisa: " + detaildata.getLisatud()

                            // change the color of the tehtud background
                            function getColor(){
                                var strl = (detaildata.getKogus()).split(' ');
                                //console.log(parseInt(strl[0]) + parseInt(strl[2]), " kogus1")
                                if (detaildata.getTehtud() <= 0 ) return red;
                                if (detaildata.getTehtud() > 0 && detaildata.getTehtud() < strl[0]) return orange;
                                if (detaildata.getTehtud() >= strl[0] && detaildata.getTehtud() <= (parseInt(strl[0]) + parseInt(strl[2]))) return green;
                                if (detaildata.getTehtud() > (parseInt(strl[0]) + parseInt(strl[2]))) return blue;
                            }
                            reckogus.color = getColor()
                            rectehtud.color = getColor()

                            tehtudid.text = "Tehtud: " + detaildata.getTehtud()

                           timerid.start()
                        }

                    }
                }
            }


            Rectangle {
                anchors.right: parent.right
                height: parent.height
                width: parent.width/2

                color: "white"
                Text {
                    id: add_text_id
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    text: qsTr("Lisa: 0")
                    font.pixelSize: 40

                }
            }





        }


        Rectangle {
            id: amountgrid_id
            anchors.top: saveandadd_id.bottom
            width: parent.width
            anchors.bottom: parent.bottom
            color: "white"
            /*gradient: Gradient {
                GradientStop { position: 1.0; color: "#0097e6" }
                GradientStop { position: 0.0; color: "#B2DFF7" }
            }*/
           // color: "lightgrey"



            GridView {
                id: grid
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenterOffset: 80
                anchors.verticalCenter: parent.verticalCenter
                width: 1200
                height: 375

                clip: true
                cellWidth: 200; cellHeight: 120
                focus: true


                model: ListModel {
                    ListElement { name: "1" }
                    ListElement { name: "5" }
                    ListElement { name: "10" }
                    ListElement { name: "25" }
                    ListElement { name: "50" }
                    ListElement { name: "100" }
                    ListElement { name: "-1" }
                    ListElement { name: "-5" }
                    ListElement { name: "-10" }
                    ListElement { name: "-25" }
                    ListElement { name: "-50" }
                    ListElement { name: "-100" }
                }



                delegate: Component {
                     id: contactsDelegate
                     Rectangle {
                         id: wrapper
                         width: 193
                         height: 113
                         border.color: "#C9D9E2"

                         color: "white"//GridView.isCurrentItem ? "#0097e6" : "white"
                         radius: 5
                         Text {
                             id: toopinktextfield
                             anchors { centerIn: parent }
                             text: name
                             font.pixelSize: 35
                             color: "black"//wrapper.GridView.isCurrentItem ? "white" : "black"
                         }
                         MouseArea {

                             property int count: 0
                             onPressedChanged: {
                                 if (count == 1) {
                                     toopinktextfield.color = "black"
                                     wrapper.color = "white"
                                     count = 0;
                                 } else {
                                     wrapper.color = "#0097e6"
                                     toopinktextfield.color = "white"
                                     count++;
                                 }
                             }

                             anchors.fill: parent
                             onClicked: {
                                 //console.log("item", index, name)
                                 parent.GridView.view.currentIndex = index

                                 detaildata.setLisatud(name)


                                 add_text_id.text = "Lisa: "  + detaildata.getLisatud()

                             }

                         }
                     }


            }



            }
        }
/*
        Rectangle {
            visible: false
            width: parent.width
            height: 0;


            Button {
                id: button2
                anchors.fill: parent
                height: 200
                width: 40
                text:"Salvesta"
                font.pixelSize: 16
                background: Rectangle {
                            anchors.verticalCenter: parent.verticalCenter
                            anchors.horizontalCenter: parent.horizontalCenter
                            implicitWidth: 200
                            implicitHeight: 50
                            color: button2.down ? "#d6d6d6" : "#f6f6f6"
                            border.color: "#26282a"
                            border.width: 1
                            radius: 4
                        }
                onClicked: {
                    detaildata.updateData(detaildata.getDetailid(), workbenchdata.getBenchName(), detaildata.getTehtud())
                    //console.log("salvesta andmed - > ", "id: "+detaildata.getDetailid(), "tehtud: "+detaildata.getTehtud(), "kogus: "+detaildata.getKogus(), "benchid: "+workbenchdata.getBenchName())
                    //console.log("discard popup")
                    frompopup = true;
                    blurvis = false;
                    detailpopupid.visible = false
                    detailpopupid.focus = false
                    dataviewid.focus = true
                    dataviewid.visible = true
                }

            }
        }*/

    }




}

