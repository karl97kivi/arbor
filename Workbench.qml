import QtQuick 2.0
import QtQuick.Controls 2.5
import WBDataModel 1.0
import WB 1.0
import app.wb 1.0


Rectangle {
    z:1
    id: wbrect
    anchors.fill: parent
    //color: "pink"
    onActiveFocusChanged: {
        console.log("workbench active", activedata.getUserKood());

        WBlist.refreshWBlist(activedata.getUserKood());
        listmodel.list = WBlist

        if (!focus) {
            console.log("focus false")
            dataviewid.focus = true;
            dataviewid.visible = true;
        }

    }
    onFocusChanged: {
        console.log("Focus changed /called Workbench.qml");
    }

    WBHandler {
        id: wbhandler
        onConfirmButtonChanged: {

            //dataviewid.focus = true;

            console.log("bench selected", workbenchdata.getBenchName())
        }
    }
    Rectangle {
        id: workbenchtitle

        anchors.top: parent.top
        anchors.topMargin: 50
        anchors.horizontalCenter: parent.horizontalCenter
        Text {
            anchors { centerIn: parent }
            id: titleText
            text: qsTr("Vali Tööpink")
            font.pixelSize: 45
            color: "#0097e6"
        }
    }
    Rectangle {
        id: logoutrec
        color: "red"
        anchors.top: parent.top
        anchors.topMargin: 50
        anchors.right:parent.right
        width: 100
        Button {
            id: logoutButton
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.right
            anchors.rightMargin: 24
            text: "Logi välja"

            font.pixelSize: 24
            background: Rectangle {
                implicitHeight: 40
                implicitWidth: 100
                border.width: 1
                color: button.down ? "#d6d6d6" : "#f6f6f6"
                border.color: "#C9D9E2"
                radius: 5
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    workbenchdata.setSelectedBenchId(0)
                    workbenchdata.setActiveBench(activedata.getUserKood())
                   loginid.visible = true
                   loginid.focus = true
                   workbenchid.visible = false
                   workbenchid.focus = false
                   dataviewid.visible = false



                }
            }
        }
    }

    Rectangle  {
        id: gridrec

        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: workbenchtitle.bottom
        anchors.topMargin: 100
        //anchors.bottom: save_button_id.top
        border.color: "#C9D9E2"
        radius: 10
        width: 930
        height: 430
        ScrollView {
            id: gridScroll
            anchors.fill: parent
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.leftMargin: 15
            anchors.rightMargin: 15
            anchors.topMargin: 15
            anchors.bottomMargin: 15

            GridView {
                id: grid
                //anchors.bottomMargin: 0
                anchors.fill: parent

                clip: true
                cellWidth: 180; cellHeight: 120
                focus: true


                model: WBListmodel {
                    id: listmodel

                    list: WBlist

                }
                delegate: Component {
                     id: contactsDelegate
                     Rectangle {
                         id: wrapper
                         width: 170
                         height: 110
                         color: "white"// GridView.isCurrentItem ? "#0097e6" : "white"
                         border.color: "#C9D9E2"
                         border.width: 3
                         radius: 5
                         Text {
                             id: toopinktextfield
                             anchors.centerIn:  parent.Center
                             text: name
                             font.pixelSize: 25
                             color: "black"
                             width: wrapper.width - 5
                             height: wrapper.height - 5
                             wrapMode: Text.Wrap // wrapper.GridView.isCurrentItem ? "white" : "black"
                             horizontalAlignment:Text.AlignHCenter
                             verticalAlignment:Text.AlignVCenter
                         }
                         MouseArea {

                             property int count: 0
                             onPressedChanged: {
                                 if (count == 1) {
                                     toopinktextfield.color = "black"
                                     wrapper.color = "white"
                                     count = 0;
                                 } else {
                                     wrapper.color = "#0097e6"
                                     toopinktextfield.color = "white"
                                     count++;
                                 }
                             }

                             anchors.fill: parent
                             onClicked: {
                                 wbhandler.selectedBench = toopinktextfield.text
                                 workbenchdata.setSelectedBenchName(toopinktextfield.text);
                                 workbenchdata.setSelectedBenchId(id)
                                 parent.GridView.view.currentIndex = index
                                 workbenchdata.setActiveBench(activedata.getUserKood())
                                 frompopup = false
                                 workbenchid.visible = false
                                 workbenchid.focus = false
                                 loginid.visible = false;
                                 wbhandler.confirmButton = true

                             }
                         }
                     }

                 }
            }
        }

        Rectangle {
            visible: false
            anchors.top: gridrec.bottom
            anchors.right: parent.right
            anchors.left: parent.left
            anchors.bottom: parent.bottom
            Button {
                id: button
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 80
                text: qsTr("Kinnita")
                background: Rectangle {
                    implicitHeight: 70
                    implicitWidth: 300
                    border.width: 1
                    color: button.down ? "#d6d6d6" : "#f6f6f6"
                    border.color: "#C9D9E2"
                    radius: 5
                }
                onClicked: {
                    console.log("userdata in WORKBENCH view: ", userdata.getUser(), "bench: ", workbenchdata.getBenchName());
                    frompopup = false;
                    wbhandler.confirmButton = true
                }
            }
        }




    }
    Rectangle {
        id: save_button_id
        anchors.top: gridrec.bottom
        width: parent.width
        height: 100
        anchors.bottom: parent.bottom
        //color: "blue"
        Rectangle {
            id: refresh_button_wrapper
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            width: 300
            height: 75
            border.color: "#0097e6"
            border.width: 2
            color: "white"
            radius: 10
            Text {
                id: save_button_text_id
                anchors { centerIn: parent }
                text: "Värskenda"
                font.pixelSize: 40
                color: "black"
            }
            MouseArea {

                property int count: 0
                onPressedChanged: {
                    if (count == 1) {
                        save_button_text_id.color = "black"
                        refresh_button_wrapper.color = "white"
                        count = 0;
                    } else {
                        refresh_button_wrapper.color = "#0097e6"
                        save_button_text_id.color = "white"
                        count++;
                    }
                }

                anchors.fill: parent
                onClicked: {
                    console.log("workbench view data refreshed. User code: ", activedata.getUserKood());
                    WBlist.refreshWBlist(activedata.getUserKood());
                    listmodel.list = WBlist
                }

            }
        }

    }


}
