#include "detaildata.h"
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>

DetailData::DetailData(QObject *parent) : QObject(parent)
{
    m_kogus = "0";
    m_tehtud = 0;
    m_lisatud = 0;
}

QString DetailData::updateData(QString userid,QString detailid, QString bench, QString tehtud) {

    qDebug() << "updated data " << detailid << bench << tehtud << userid;




    QByteArray bytes = server.dataGET("getTehtudSissekanne", "/17/63");

    QJsonObject jsondata;
    jsondata.insert("detailid", detailid);
    jsondata.insert("toopinkid", bench);
    jsondata.insert("kogus", tehtud);

    QByteArray jsonbyte = QJsonDocument(jsondata).toJson(QJsonDocument::JsonFormat::Compact);
    QString str = QString::fromUtf8(bytes.data(), bytes.size());
    //qDebug() << "sissekanne: value: " << str;
    QUrlQuery query;
    query.addQueryItem(QString("tehtud").toUtf8(), QString(jsonbyte).toUtf8() );
    if (server.checkInternetConnection() == "OK") {
        server.dataPOST("updateTehtud", query);

        return "success";
    } else if (server.checkInternetConnection() == "NOT OK") {
        qDebug() << "No internet connection (updateData)";
    }
    return "No success updating bench data";
}

QString DetailData::findImages(QString detail_dnr)
{
    QByteArray bytes = server.dataGET("findAllImages/", detail_dnr);
    QString str = QString::fromUtf8(bytes.data(), bytes.size());

    QJsonDocument document = QJsonDocument::fromJson(bytes);
    QJsonArray array = document.array();
    QList<QString> images;
    foreach (const QJsonValue &v, array) {

        images.append(v.toObject().value("filename").toString());
    }

    if (images.isEmpty()) return "";
    qDebug() << "images list: " << images.first() << str;

    return QString("http://192.168.1.200/download/" + images.first());
}





void DetailData::setClickedDetail(QString detail) {
    m_clickedDetail = detail;
}

void DetailData::setKogus(QString kogus){
    m_kogus = kogus;
}
void DetailData::setTehtud(int tehtud) {
    m_tehtud += tehtud;
}

void DetailData::setLisatud(int lisatud)
{
    m_lisatud += lisatud;
}
void DetailData::setDetailid(QString data) {
    m_detailid = data;
}

void DetailData::setTehtudToZero() {
    m_tehtud = 0;
}

void DetailData::setLisatudToZero()
{
    m_lisatud = 0;
}


QString DetailData::getKogus() {
    return m_kogus;
}

int DetailData::getTehtud() {
    return m_tehtud;
}

int DetailData::getLisatud()
{
    return m_lisatud;
}

QString DetailData::getClickedDetail() {
    return m_clickedDetail;
}

QString DetailData::getDetailid() {
    return m_detailid;
}
