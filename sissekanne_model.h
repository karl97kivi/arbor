#ifndef SISSEKANNE_MODEL_H
#define SISSEKANNE_MODEL_H

#include <QAbstractListModel>
#include "list_entry.h"



class sissekanne_model : public QAbstractListModel
{
    Q_OBJECT
    //Q_PROPERTY(List_sissekanne *sissekanded READ sissekanded WRITE setSissekanded NOTIFY sissekandedChanged)

public:
    explicit sissekanne_model(QObject *parent = nullptr);

    // Header:
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    EntryList *sissekanded() const;


private:
    void setSissekanded(EntryList *sissekanded);
    EntryList *m_sissekanded;


public slots:


signals:
    void sissekandedChanged();
};

#endif // SISSEKANNE_MODEL_H
