import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import ProductListModel 1.0
//import ProductList 1.0
//import app.model.tableview 0.1
import app.ListTableModel 1.0
//import app.customTableModel 0.1
import app.DataTableModel 1.0
Rectangle {
    id: dvrec


    anchors.fill: parent
    onActiveFocusChanged: {
        console.log("dataview active", activedata.getSelectedToodeID());
        userid.text =  userdata.getUser()
        benchid.text =  workbenchdata.getBenchName()
        if (!frompopup) listDataView.currentIndex = 0;
        if (!frompopup) {
            console.log("toode set to 0");
            activedata.setSelectedToodeID(0);
            listtablemodelid.count = 0;
            frompopup = true;
        }


        Clist.refreshTooteData(workbenchdata.getBenchName());
        //if (!frompopup) dvtmid2.setInitToode();
        //frompopup = true;
        productlistid.productList = Clist;
        //dvtmid.cList = Clist;
        //activedata.setSelectedToodeID(0);

        listtablemodelid.tooted = Clist;


    }


    property DataTableModel dtm: dvtmid
    property string tootename: ""

    RowLayout {
       id: topmenubar
       anchors.top: parent.top
       anchors.right: parent.right
       anchors.left: parent.left
       height: 100
       Rectangle {
           anchors.fill: parent
           gradient: Gradient {
               GradientStop { position: 0.0; color: "#0097e6" }
               GradientStop { position: 1.0; color: "#B2DFF7" }
           }
           Label {
               id: userid
               anchors.verticalCenter: parent.verticalCenter
               anchors.left: parent.left
               anchors.leftMargin: 24
               font.pixelSize: 24
               text: userdata.getUser()
               color: "white"

           }

           Label {
               id: benchid
               anchors.horizontalCenter: parent.horizontalCenter
               anchors.verticalCenter: parent.verticalCenter
               text: ""//workbenchdata.getBenchName()

               font.pixelSize: 50
               color: "white"
               MouseArea {
                   anchors.fill: parent
                   onClicked: {
                       //activedata.setSelectedToodeID("0");
                       workbenchdata.setSelectedBenchId(0)
                       workbenchdata.setActiveBench(activedata.getUserKood())
                      workbenchid.visible = true
                       workbenchid.focus = true
                      dataviewid.visible = false
                       dataviewid.focus = false
                   }
               }
           }


           Button {
               id: logoutButton
               anchors.verticalCenter: parent.verticalCenter
               anchors.right: parent.right
               anchors.rightMargin: 24
               text: "Logi välja"

               font.pixelSize: 24
               background: Rectangle {
                   implicitHeight: 40
                   implicitWidth: 100
                   border.width: 1
                   color: button.down ? "#d6d6d6" : "#f6f6f6"
                   border.color: "#C9D9E2"
                   radius: 5
               }
               MouseArea {
                   anchors.fill: parent
                   onClicked: {
                      loginid.visible = true
                      dataviewid.visible = false
                       workbenchdata.setSelectedBenchId(0)
                       workbenchdata.setActiveBench(activedata.getUserKood())

                   }
               }
           }


       }


    }



    ColumnLayout {
        id: productlist
        anchors.top: topmenubar.bottom
        anchors.left: parent.left
        anchors.right: tableview.left
        anchors.bottom: parent.bottom
        width: 250
        clip:true
        Rectangle {
            id: recproductlist
            anchors.fill: parent




            ListView {
                id: listDataView
                anchors.fill: parent
                anchors.top: parent.top
                anchors.topMargin: 16
                spacing: 5

                orientation: ListView.Vertical
                model: ProductListmodel {
                    id: productlistid
                    productList: Clist
                }

                delegate: Component {
                    id: listDelegate

                    Rectangle {
                        id: wrapper
                        anchors.right: parent.right
                        anchors.rightMargin: 16
                        anchors.left: parent.left
                        anchors.leftMargin: 16
                        border.color: "#D3D3D3"
                        border.width: 2
                        height: 100
                        color: ListView.isCurrentItem ? "#0097e6" : "white"
                        radius: 5
                        Text {
                            id: toopinktextfield
                            anchors {
                                left: parent.left
                                leftMargin:16
                            verticalCenter: parent.verticalCenter}
                            text: name
                            font.pixelSize: 18
                            width: wrapper.width - 15
                            //height: wrapper.height - 5
                            wrapMode: Text.Wrap
                            color: wrapper.ListView.isCurrentItem ? "white" : "black"
                        }
                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                //console.log("peaks olema id: " + name_id + name);
                                activedata.setSelectedToodeID(name_id)
                                console.log("peaks olema id: " + name_id + name, activedata.getSelectedToodeID());
                                //dvtmid.setSelToode(activedata.getSelectedToodeID())
                                userid.text =  userdata.getUser()
                                benchid.text =  workbenchdata.getBenchName()
                                listDataView.currentIndex = index
                                tootename = toopinktextfield.text
                                //dvtmid.selectedToode = toopinktextfield.text
                                //console.log("active id: ", dvtmid.selectedToode)
                                //dvtableview.selectedToode = detail;
                                listtablemodelid.count = index//activedata.getSelectedToodeID()
                                //listtablemodelid.deleteRows()
                                //listtablemodelid.rowCount()
                              // listtablemodelid.rownum = 5
                               listtablemodelid.tooted = Clist
                               // listviewid.update()
                                //listtablemodelid.submit()

                            }
                        }
                    }

                }
            }

            Rectangle {
                anchors.top: parent.top
                anchors.topMargin: 8
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 8
                anchors.right: parent.right
                anchors.rightMargin: 5
                width: 3
                color: "#ddd"
            }

        }
    }

    Rectangle {
        id: tableview
        anchors.top: topmenubar.bottom
        anchors.left: productlist.right
        //anchors.leftMargin: 8
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        //color: "red"

        ListView {
            id: listviewid
            anchors.fill: parent
            spacing: 20
            clip: true
            model: ListTableModel {
                id: listtablemodelid
                rownum: 4
                tooted: Clist
            }


            delegate: Component {
                id:listtabledelegateid
                Rectangle {
                    id: wrapper

                    width: listviewid.width
                    height: wrapper.height = recalamtoodeid.height + dvtableview.contentHeight

                    Component.onCompleted: {
                        //console.log("current list index ",index)
                        wrapper.height = recalamtoodeid.height + dvtableview.contentHeight
                    }


                    Rectangle {
                        id: recalamtoodeid
                        anchors {

                            left: parent.left
                            top: parent.top
                        }
                        width: 200
                        height: 40

                        Text {
                            anchors {
                                left: parent.left
                                leftMargin: 30
                                verticalCenter: recalamtoodeid.verticalCenter

                            }
                            width:40
                            id: tkarvid
                            text: tk + "tk"
                            font.pixelSize: 22
                        }
                        Text {
                            anchors {
                                leftMargin: 30
                                left: tkarvid.right
                               verticalCenter: recalamtoodeid.verticalCenter
                            }
                            id: alamtootenimiid
                            text: name
                            font.pixelSize: 22
                        }

                    }

                    TableView {
                        id: dvtableview
                        anchors.top:recalamtoodeid.bottom
                        anchors.right: parent.right
                        anchors.left: parent.left
                        anchors.bottom: parent.bottom

                        width: listviewid.width
                        //height: dvtableview.count * 25

                        rowSpacing: 2
                        clip: true
                        property int listIndex: index


                      model: DataTableModel {
                          alamIndex: dvtableview.listIndex
                          id: dvtmid2
                          cList: Clist
                          selectedToode: activedata.getSelectedToodeID()

                      }



                      delegate: Rectangle {
                          id: tableviewdelegate
                          implicitWidth: widthrole
                          implicitHeight: 40
                          color: heading
                          Rectangle {
                              width: widthrole
                              anchors.bottom: parent.bottom
                              height: 1
                              color: "#e0e0e0"
                          }
                          Text {
                              id: lahe
                              font.pixelSize: title
                              anchors.centerIn: parent
                              text: tabledata
                              height: tableviewdelegate.height + 5
                              width: widthrole - 5
                              wrapMode: Text.WordWrap
                              horizontalAlignment:Text.AlignHCenter
                              verticalAlignment:Text.AlignVCenter

                          }
                          MouseArea {
                              anchors.fill: parent
                              onClicked: {
                                   console.log("tableview row: ", row )
                                  if (row != 0) {
                                      detaildata.setClickedDetail(dvtmid2.getData(row, "dnr"))
                                      detaildata.setKogus(dvtmid2.getData(row, "kogus"))
                                      detaildata.setDetailid(dvtmid2.getData(row, "id"))
                                      detaildata.setTehtudToZero()
                                      detaildata.setTehtud(dvtmid2.getData(row, "tehtud"))

                                      DDStruct.dnr = dvtmid2.getData(row, "dnr");
                                      DDStruct.name = dvtmid2.getData(row, "detail");
                                      DDStruct.pkilp = dvtmid2.getData(row, "pkilp");
                                      DDStruct.mmoot = dvtmid2.getData(row, "mmoot");
                                      DDStruct.pmoot = dvtmid2.getData(row, "pmoot");
                                      DDStruct.tk = dvtmid2.getData(row, "tk");

                                      //blurvis = true;
                                      //dataviewid.focus = false;
                                      dataviewid.visible = false;
                                      detailpopupid.visible = true
                                      detailpopupid.focus = true
                                      //detailpopupid.forceActiveFocus()
                                  }


                              }
                          }


                      }
                   }

                    /*MouseArea {
                        anchors.fill: parent
                        onClicked: {

                            console.log("active id: ", index )


                        }
                    }*/
                }
            }

        }
    }
/*
    ColumnLayout {
        visible: false
        id: tableview1
        anchors.top: topmenubar.bottom
        anchors.left: productlist.right
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        Rectangle {
            anchors.fill: parent
            TableView {
                id: dvtableview1
                anchors.fill: parent
                rowSpacing: 1
                clip: true



              model: DVTableModel {
                  id: dvtmid1
                  cList: Clist

              }

              delegate: Rectangle {
                    id: tableviewdelegate1
                  implicitWidth: {
                      if (title) return 50
                      //if (column == 0 && row == 0) return 10
                      if (column == 2) {
                          return 80;
                      }
                      if (column == 5 ) {
                          return 50
                       } else {
                          return 130;
                      }
                  }


                  color: heading ? "#e0e0e0": "white"
                  implicitHeight: 40
                  Text {
                      id: lahe1

                      font.pixelSize: title ? 16 : 11
                      anchors.centerIn: parent
                      text: tabledata

                  }
                  MouseArea {
                      anchors.fill: parent
                      onClicked: {
                        //console.log("item clicked:", tabledata, "row index: ", row, "getdata: ", dvtmid.getData(row, "tehtud"))
                        detaildata.setClickedDetail(dvtmid.getData(row, "dnr"))
                        detaildata.setKogus(dvtmid.getData(row, "kogus"))
                        detaildata.setDetailid(dvtmid.getData(row, "id"))
                        detaildata.setTehtudToZero()
                        detaildata.setTehtud(dvtmid.getData(row, "tehtud"))

                        DDStruct.dnr = dvtmid.getData(row, "dnr");
                        DDStruct.name = dvtmid.getData(row, "detail");
                        DDStruct.pkilp = dvtmid.getData(row, "pkilp");
                        DDStruct.mmoot = dvtmid.getData(row, "mmoot");
                        DDStruct.pmoot = dvtmid.getData(row, "pmoot");
                        DDStruct.tk = dvtmid.getData(row, "tk");
                          //DDStruct.detailDataStruct.dnr = dvtmid.getData(row, "dnr");
                          //DDStruct.detailDataStruct.name = dvtmid.getData(row, "detail");
                          //DDStruct.detailDataStruct.pkilp = dvtmid.getData(row, "pkilp");
                          //DDStruct.detailDataStruct.mmoot = dvtmid.getData(row, "mmust");
                          //DDStruct.detailDataStruct.pmoot = dvtmid.getData(row, "mpuhas");
                          //DDStruct.detailDataStruct.tk = dvtmid.getData(row, "tk");

                        blurvis = true;
                        detailpopupid.visible = true
                        detailpopupid.focus = true
                        detailpopupid.forceActiveFocus()

                      }
                  }


              }
           }

        }
    }*/







}


