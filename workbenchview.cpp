#include "workbenchview.h"



workbenchview::workbenchview(QObject *parent) : QObject(parent)
{

}

bool workbenchview::confirmButton()
{
    return m_confirmButton;
}

QString workbenchview::selectedBench()
{
    return m_selectedBench;
}



void workbenchview::setConfirmButton(bool buttonClicked)
{
    qDebug() << "confirm button clicked" << buttonClicked;
    emit confirmButtonChanged();
}

void workbenchview::setSelectedBench(QString selected)
{
    qDebug() << "selected bench: " << selected;
    emit selectedBenchChanged();
}
