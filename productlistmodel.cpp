#include "productlistmodel.h"
#include "dvtablemodel.h"
#include <QObject>


ProductListModel::ProductListModel(QObject *parent)
    : QAbstractListModel(parent), mProductList(nullptr)
{
}



int ProductListModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid() || !mProductList)
        return 0;
    //qDebug() << "ROW COUNT for product list " <<mProductList->listTooted().size();
    Q_ASSERT(mProductList->listTooted().size() >= 0);
    return mProductList->listTooted().size();
}

QVariant ProductListModel::data(const QModelIndex &index, int role) const
{
    //Q_ASSERT( 2+index.row() > mProductList->listTooted().size());
    switch (role) {
    case IdRole:
        if (!index.isValid() || !mProductList)
            return QVariant();
        //qDebug() << "passing through datafunc "<<mProductList->listTooted().size();

        if (index.row() < mProductList->listTooted().size()) {
            //qDebug() << "idrole " << mProductList->listTooted()[index.row()].toote_id;
             return QString(mProductList->listTooted()[index.row()].toote_id);
        } else {
            return " ";
        }
    case NameRole:
        if (!index.isValid() || !mProductList)
            return QVariant();
        //qDebug() << "passing through datafunc "<<mProductList->listTooted().size();

        if (index.row() < mProductList->listTooted().size()) {
             return QString::number(mProductList->listTooted()[index.row()].rownr) + ". "+ QString(mProductList->listTooted()[index.row()].tootenimi);
        } else {
            return " ";
        }

    default:
        break;
    }


    return QVariant("");


}

bool ProductListModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    return true;
}

QHash<int, QByteArray> ProductListModel::roleNames() const
{
    QHash<int, QByteArray> names;
    names[IdRole] = "name_id";
    names[NameRole] = "name";

    return names;
}

DVtableList *ProductListModel::productList() const
{
    return mProductList;
}

void ProductListModel::setProductList(DVtableList *productList)
{
    //qDebug() << "PRODUCT LIST setting (rowcount)" <<productList->listTooted().size() ;
    for ( int i = 0; i < productList->listTooted().size(); i++) {
        //qDebug() << "toode in setter: " << productList->listTooted()[i].tootenimi;
    }

    mProductList = productList;
    beginResetModel();
    endResetModel();
    emit dataChanged(createIndex(0,0), createIndex(20,0));

}
