import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12

Rectangle {
    anchors.fill: parent
    onActiveFocusChanged: {
        imageid.source = detaildata.findImages(DDStruct.dnr)
                //"http://192.168.1.200/download/DA-85-1.png"
    }
   Rectangle {
       id: toolbarid
       anchors.top: parent.top
       anchors.right: parent.right
       anchors.left: parent.left
       anchors.bottom: imageviewid.top
       height: 100
       color: "grey"
       Button {
           id: logoutButton
           anchors.verticalCenter: parent.verticalCenter
           anchors.right: parent.right
           anchors.rightMargin: 24
           text: "Tagasi"

           font.pixelSize: 24
           background: Rectangle {
               implicitHeight: 40
               implicitWidth: 100
               border.width: 1
               color: button.down ? "#d6d6d6" : "#f6f6f6"
               border.color: "#C9D9E2"
               radius: 5
           }
           MouseArea {
               anchors.fill: parent

               onClicked: {
                   //activedata.setSelectedToodeID("0");
                   imageview_id.focus = false
                   imageview_id.visible = false
                   detailpopupid.visible = true
                   detailpopupid.focus = true

               }
           }
       }
   }
   Rectangle {
       id: imageviewid
       anchors.top: toolbarid.bottom
       anchors.right: parent.right
       anchors.left: parent.left
       anchors.bottom: parent.bottom
       Image {
           id: imageid
           anchors.fill: parent
           source: "http://192.168.43.124/download/DA-85.png"
       }
   }
}
