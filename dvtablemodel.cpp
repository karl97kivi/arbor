#include "dvtablemodel.h"
#include "QString"
#include <math.h>
DVTableModel::DVTableModel(QObject *parent)
    : QAbstractTableModel(parent)
{
}

DVTableModel::~DVTableModel()
{
    tooteid = 0;
}

int DVTableModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    qDebug() << "should not be called";
    return 20;
}

int DVTableModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return 8;
}

QVariant DVTableModel::data(const QModelIndex &index, int role) const
{
    Q_ASSERT(mCtList->listTooted().size() > 0);
    int alampos = 0;
    int detpos = 0;
    int columnpos = 1;
    int last_ats = 0;
    int pos_last_ats = 0;
    if (!index.isValid() || !mCtList)
        return QVariant();

    int toodeid = tooteid;

    switch (role) {
    case TableDataRole:


        if (index.row() > -1) {

            for (int f = 0; f < mCtList->listTooted()[toodeid].alamtooted.size(); f++) {

                if (index.row() == alampos && index.column() == 0) {
                    return mCtList->listTooted()[toodeid].alamtooted[f].alamtootenimi;
                }

                if (index.row() == alampos) return QVariant("");
                int ats = mCtList->listTooted()[toodeid].alamtooted[f].detailid.size();
                alampos = alampos + ats + 2;

            }
            for (int f = 0; f < mCtList->listTooted()[toodeid].alamtooted.size(); f++) {

                if (index.row() == columnpos) {
                    for (int i=0; i < 8; i++) {
                        if (index.column() == i) {

                            return columnTitles[i];
                        }
                    }
                }

                int ats = mCtList->listTooted()[toodeid].alamtooted[f].detailid.size();
                columnpos = columnpos + ats + 2;

            }
            for (int f = 0; f < mCtList->listTooted()[toodeid].alamtooted.size(); f++) {

                int ats = mCtList->listTooted()[toodeid].alamtooted[f].detailid.size();

                for (int h = 0; h < ats ; h++) {
                    for (int g = 0; g < 8; g++) {

                        if (index.row() == ((detpos +f + 2) + h  + 0) && index.column() == 0) return mCtList->listTooted()[toodeid].alamtooted[f].detailid[h].dnr;
                        if (index.row() == ((detpos +f + 2) + h  + 0) && index.column() == 1) return mCtList->listTooted()[toodeid].alamtooted[f].detailid[h].detail;
                        if (index.row() == ((detpos +f + 2) + h  + 0) && index.column() == 2) return mCtList->listTooted()[toodeid].alamtooted[f].detailid[h].kilp;
                        if (index.row() == ((detpos +f + 2) + h  + 0) && index.column() == 3) return mCtList->listTooted()[toodeid].alamtooted[f].detailid[h].must;
                        if (index.row() == ((detpos +f + 2) + h  + 0) && index.column() == 4) return mCtList->listTooted()[toodeid].alamtooted[f].detailid[h].puhas;
                        if (index.row() == ((detpos +f + 2) + h  + 0) && index.column() == 5) return mCtList->listTooted()[toodeid].alamtooted[f].detailid[h].tk;

                        int vaja = mCtList->listTooted()[toodeid].alamtooted[f].detailid[h].tk * mCtList->listTooted()[toodeid].alamtooted[f].alamtootekogus;
                        int juurde = (vaja - mCtList->listTooted()[toodeid].alamtooted[f].detailid[h].pv);
                        int juurde_viis = (int) round(juurde * 0.05 + mCtList->listTooted()[toodeid].alamtooted[f].detailid[h].lisa_juurde);
                        if (index.row() == ((detpos +f + 2) + h  + 0) && index.column() == 6) return QString::number(juurde) + QString(" + 1") + QString::number(juurde_viis);
                        if (index.row() == ((detpos +f + 2) + h  + 0) && index.column() == 7) return mCtList->listTooted()[toodeid].alamtooted[f].detailid[h].tehtud;
                    }
                }

                detpos = detpos + ats +1;

                last_ats = ats;

            }

        }
          return QVariant("");
    case HeadingRole:
        if (index.row() > -1) {

            for (int f = 0; f < mCtList->listTooted()[toodeid].alamtooted.size(); f++) {

                if (index.row() == alampos && index.column() == 0) {
                    return false;
                }

                if (index.row() == alampos) return false;
                int ats = mCtList->listTooted()[toodeid].alamtooted[f].detailid.size();
                alampos = alampos + ats + 2;

            }
            for (int f = 0; f < mCtList->listTooted()[toodeid].alamtooted.size(); f++) {

                if (index.row() == columnpos) {
                    for (int i=0; i < 8; i++) {
                        if (index.column() == i) {

                            return true;
                        }
                    }
                }

                int ats = mCtList->listTooted()[toodeid].alamtooted[f].detailid.size();
                columnpos = columnpos + ats + 2;

            }
            for (int f = 0; f < mCtList->listTooted()[toodeid].alamtooted.size(); f++) {

                int ats = mCtList->listTooted()[toodeid].alamtooted[f].detailid.size();

                for (int h = 0; h < ats ; h++) {
                    for (int g = 0; g < 8; g++) {

                        if (index.row() == ((detpos +f + 2) + h  + 0) && index.column() == 0) return false;
                        if (index.row() == ((detpos +f + 2) + h  + 0) && index.column() == 1) return false;
                        if (index.row() == ((detpos +f + 2) + h  + 0) && index.column() == 2) return false;
                        if (index.row() == ((detpos +f + 2) + h  + 0) && index.column() == 3) return false;
                        if (index.row() == ((detpos +f + 2) + h  + 0) && index.column() == 4) return false;
                        if (index.row() == ((detpos +f + 2) + h  + 0) && index.column() == 5) return false;
                        if (index.row() == ((detpos +f + 2) + h  + 0) && index.column() == 6) return false;
                        if (index.row() == ((detpos +f + 2) + h  + 0) && index.column() == 7) return false;
                    }
                }

                detpos = detpos + ats +1;
                last_ats = ats;

            }

        }
        return false;
    case TitleRole:
        for (int f = 0; f < mCtList->listTooted()[toodeid].alamtooted.size(); f++) {

            if (index.row() == alampos && index.column() == 0) {
                return true;
            }
            int ats = mCtList->listTooted()[toodeid].alamtooted[f].detailid.size();
            alampos = alampos + ats + 2;


        }

        return false;
      default:
          break;
  }
    return QVariant();
}
QHash<int, QByteArray> DVTableModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[TableDataRole] = "tabledata";
    roles[HeadingRole] = "heading";
    roles[TitleRole] = "title";
    return roles;
}

QString DVTableModel::getData(int row, QString par)
{
    /*if (data(createIndex(row, 1), 0).toString() == "detail") return true;
    else return false;*/
    int detpos = 0;

    int last_ats = 0;
    for (int f = 0; f < mCtList->listTooted()[tooteid].alamtooted.size(); f++) {

        int ats = mCtList->listTooted()[tooteid].alamtooted[f].detailid.size();

        for (int h = 0; h < ats ; h++) {
            for (int g = 0; g < 8; g++) {

                if (row == ((detpos +f + 2) + h  + 0)) {
                    //qDebug() << "getData func, detail: " << mCtList->listTooted()[tooteid].alamtooted[f].detailid[h].dnr;
                    if (par == "dnr") return mCtList->listTooted()[tooteid].alamtooted[f].detailid[h].dnr;
                    if (par == "detail") return mCtList->listTooted()[tooteid].alamtooted[f].detailid[h].detail;
                    if (par == "pkilp") return mCtList->listTooted()[tooteid].alamtooted[f].detailid[h].kilp;
                    if (par == "mmoot") return mCtList->listTooted()[tooteid].alamtooted[f].detailid[h].must;
                    if (par == "pmoot") return mCtList->listTooted()[tooteid].alamtooted[f].detailid[h].puhas;
                    if (par == "tk") return QString::number(mCtList->listTooted()[tooteid].alamtooted[f].detailid[h].tk);
                    if (par == "tehtud") return QString::number(mCtList->listTooted()[tooteid].alamtooted[f].detailid[h].tehtud);



                    int vaja = mCtList->listTooted()[tooteid].alamtooted[f].detailid[h].tk * mCtList->listTooted()[tooteid].alamtooted[f].alamtootekogus;
                    int juurde = (vaja - mCtList->listTooted()[tooteid].alamtooted[f].detailid[h].pv);
                    int juurde_viis = round(juurde * 0.05 + mCtList->listTooted()[tooteid].alamtooted[f].detailid[h].lisa_juurde);
                    if (par == "kogus") return QString::number(juurde) + QString(" + ") + QString::number(juurde_viis);
                    if (par == "id") return QString::number(mCtList->listTooted()[tooteid].alamtooted[f].detailid[h].id);
                    if (par == "num_kogus") return QString::number(juurde);
                    if (par == "num_kogus_viis") return QString::number(juurde_viis);
                }

            }
        }

        detpos = detpos + ats +1;

        last_ats = ats;

    }

    return "none";

}



void DVTableModel::setSelectedToode(QString toode)
{
    m_selectedToode = toode;
    //qDebug() << "selected toode set: " << toode;
    for (int f = 0; f < mCtList->listTooted().size(); f++) {
        if (mCtList->listTooted()[f].tootenimi == toode) {
            tooteid = f;
            emit dataChanged(createIndex(0,0), createIndex(mCtList->listTooted()[0].alamtooted.size(), 8), QVector<int>(0));
        }
    }
}

QString DVTableModel::selectedToode()
{
    return m_selectedToode;
}

void DVTableModel::setInitToode()
{
    Q_ASSERT(mCtList->listTooted().size() > 0);

    if (mCtList->listTooted().size() > 0) {
        m_selectedToode = mCtList->listTooted()[0].tootenimi;
        tooteid = 0;
        emit dataChanged(createIndex(0,0), createIndex(mCtList->listTooted()[0].alamtooted.size(), 8), QVector<int>(0));
    }

}
void DVTableModel::setCList(DVtableList * customList) {
    mCtList = customList;
    if (mCtList->listTooted().size() > 0) {
        emit dataChanged(createIndex(0,0), createIndex(mCtList->listTooted()[0].alamtooted.size(),8), QVector<int>(0));
    }
}


DVtableList *DVTableModel::cList() const
{
    return mCtList;
}
