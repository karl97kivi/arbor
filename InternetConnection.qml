import QtQuick 2.0
import QtQuick.Controls 2.5

Rectangle {
    anchors.horizontalCenter: parent.horizontalCenter
    anchors.verticalCenter: parent.verticalCenter
    width: 600
    height: 200
    border.color: "red"
    border.width: 5


    Rectangle {
        id: label_box_id
        anchors.top: parent.top
        width: parent.width
        height: 100
        color: "blue"

        Label {
            id: connection_label_id
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            font.pixelSize: 24
            text: "Interneti ühendus puudub!"
            color: "red"

        }
    }

    Rectangle {
        anchors.top: label_box_id.bottom
        width: parent.width
        height: 100
        color: "green"


    }
}
