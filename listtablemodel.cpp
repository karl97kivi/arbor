#include "listtablemodel.h"

ListTableModel::ListTableModel(QObject *parent)
    : QAbstractListModel(parent)
{
    //qDebug() << "listtablemodel objejct";
    m_count = 0;
    last_count = 0;
    m_rownum = 0;
}

QVariant ListTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    // FIXME: Implement me!
    return QVariant(" ");
}

int ListTableModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid())
        return 0;


   // qDebug() << "listtablemodel rowcount";

    //Q_ASSERT(m_tooted->listTooted().size() < 0);

    int returnable = 0;
    if (m_tooted->listTooted().size() != 0) returnable = m_tooted->listTooted()[m_count].alamtooted.size();
    return returnable; // This is going to make your young head ache in the near future

    // FIXME: Implement me!
}

QVariant ListTableModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();
   //qDebug() << "data row count " ;
    // FIXME: Implement me!

    switch (role) {
    case NameRole:


        if (index.row() < m_tooted->listTooted()[m_count].alamtooted.size()) {
            return m_tooted->listTooted()[m_count].alamtooted[index.row()].alamtootenimi;
        } else {
            return QVariant(" ");
        }
    case TkRole:

        if (index.row() < m_tooted->listTooted()[m_count].alamtooted.size()) {
            return m_tooted->listTooted()[m_count].alamtooted[index.row()].alamtootekogus;
        } else {
            return QVariant(" ");
        }
    default:
        if (index.row() < m_tooted->listTooted()[m_count].alamtooted.size()) {
            return m_tooted->listTooted()[m_count].tootenimi;
        } else {
            return QVariant(" ");
        }

    }


}

void ListTableModel::setTooted(DVtableList *list)
{
    m_tooted = list;
    qDebug() << "listtablemodel settooted" << m_tooted->listTooted().size();
    int indexForDataChange = 0;
    if (m_tooted->listTooted().size() != 0) {
        qDebug() << "listtablemodel settooted list 0";
        indexForDataChange = m_tooted->listTooted()[m_count].alamtooted.size();

    }

    beginResetModel();
    endResetModel();
    emit dataChanged(createIndex(0,0), createIndex(indexForDataChange,0));
    emit tootedChanged();
}

QHash<int, QByteArray> ListTableModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[NameRole] = "name";
    roles[TkRole] = "tk";
    return roles;
}

void ListTableModel::deleteRows()
{
    /*
    int cur = m_tooted->listTooted()[m_count].alamtooted.size();
    int count1 = m_tooted->listTooted()[last_count].alamtooted.size() -cur;

    if (count1 != 0 && count1 > 0) {
        removeRows(cur-1, count1, QModelIndex());
    }*/

    m_tooted->listTooted() = QVector<Toode>();

}

void ListTableModel::setRownum(int rownum)
{
    m_rownum = rownum;
    emit rownumChanged();
}

int ListTableModel::getRownum()
{
    return m_rownum;
}

void ListTableModel::setCount(int val)
{
    last_count = m_count;
    m_count = val;
    emit countChanged();
}

bool ListTableModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (data(index, role) != value) {
        // FIXME: Implement me!
        qDebug() << "datachanged1";
        emit dataChanged(index, index, QVector<int>() << role);
        return true;
    }

    qDebug() << "datachanged2";
    return false;
}

int ListTableModel::getCount()
{
    return m_count;
}

bool ListTableModel::removeRows(int row, int count, const QModelIndex &parent)
{
    beginRemoveRows(parent, row, row + count - 1);
    // FIXME: Implement me!
    qDebug() << "removing row";
    endRemoveRows();
    return true;
}

DVtableList *ListTableModel::tooted() const
{
    return m_tooted;
}
