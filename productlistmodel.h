#ifndef PRODUCTLISTMODEL_H
#define PRODUCTLISTMODEL_H

#include <QAbstractListModel>
#include "productlist.h"
#include "dvtablelist.h"

class ProductListModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(DVtableList *productList READ productList WRITE setProductList NOTIFY productListChanged)

public:
    explicit ProductListModel(QObject *parent = nullptr);
    enum {
        IdRole = Qt::UserRole,
        NameRole
    };


    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::DisplayRole) override;
    virtual QHash<int, QByteArray> roleNames() const override;
    DVtableList *productList() const;

signals:
    void productListChanged();
private:
    void setProductList(DVtableList * productList);
    DVtableList *mProductList;
};

#endif // PRODUCTLISTMODEL_H
