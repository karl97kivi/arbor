#include "tootedtableviewmodel.h"

#include <math.h>

TootedTableViewModel::TootedTableViewModel(QObject *parent)
    : QAbstractTableModel(parent)
{
    tooteid = 0;
    //qDebug()<< "r0" << tooteid;
}

int TootedTableViewModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    // FIXME: Implement me!
    //qDebug()<< "r1" << tooteid;
   //Q_ASSERT(mCtList->listTooted().size() > 0);
    if (mCtList->listTooted().size() == 0) {
        qDebug() << "tooted list empty";
    }
    return mCtList->listTooted()[tooteid].alamtooted[m_alamIndex].detailid.size()+1;
}

int TootedTableViewModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return 8;
    // FIXME: Implement me!
}

QVariant TootedTableViewModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();
    //qDebug()<< "r2" << tooteid;
    int alampos = 0;
    int detpos = 0;
    int columnpos = 1;
    int last_ats = 0;
    int pos_last_ats = 0;
    if (!index.isValid() || !mCtList)
        return QVariant();
    if (mCtList->listTooted().size() == 0) {
        qDebug() << "tooted list empty1";
    }
    int toodeid = tooteid;

    switch (role) {
    case TableDataRole:
        if (index.row() > -1) {
            if (index.row() == 0) {
                for (int i=0; i < columnTitles.size(); i++) {
                    if (index.column() == i) {

                        return columnTitles[i];
                    }
                }
            }
        }
        //int ats = mCtList->listTooted()[toodeid].alamtooted[0].detailid.size();

        for (int h = 0; h < mCtList->listTooted()[toodeid].alamtooted[m_alamIndex].detailid.size() ; h++) {
            if (toodeid == 3 && m_alamIndex == 0) {
                //qDebug() << mCtList->listTooted()[toodeid].alamtooted[m_alamIndex].detailid[h].dnr;
            }

            for (int g = 0; g < 6; g++) {

                if (index.row() == ((h + 1)) && index.column() == 0) return mCtList->listTooted()[toodeid].alamtooted[m_alamIndex].detailid[h].dnr;
                if (index.row() == ((h + 1)) && index.column() == 1) return mCtList->listTooted()[toodeid].alamtooted[m_alamIndex].detailid[h].detail;

                int vaja = mCtList->listTooted()[toodeid].alamtooted[m_alamIndex].detailid[h].tk * mCtList->listTooted()[toodeid].alamtooted[m_alamIndex].alamtootekogus;
                int juurde = (vaja - mCtList->listTooted()[toodeid].alamtooted[m_alamIndex].detailid[h].pv);
                int juurde_viis = (int) round(juurde * 0.05+ mCtList->listTooted()[toodeid].alamtooted[m_alamIndex].detailid[h].lisa_juurde);
                if (index.row() == ((h + 1)) && index.column() == 2) return QString::number(juurde) + QString(" + ") + QString::number(juurde_viis);
                if (index.row() == (h + 1) && index.column() == 3) {
                    if (mCtList->listTooted()[toodeid].alamtooted[m_alamIndex].detailid[h].hetkel != "0") {
                        //qDebug() << "hetkel 1";
                        return mCtList->listTooted()[toodeid].alamtooted[m_alamIndex].detailid[h].hetkel;
                    } else {
                        //qDebug() << "hetkel 0" << mCtList->listTooted()[toodeid].alamtooted[m_alamIndex].detailid[h].hetkel;
                        return " ";
                    }
                }
                if (index.row() == ((h + 1)) && index.column() == 4) return mCtList->listTooted()[toodeid].alamtooted[m_alamIndex].detailid[h].tehtud;

                if (index.row() == (h + 1) && index.column() == 5) {
                    if ( mCtList->listTooted()[toodeid].alamtooted[m_alamIndex].detailid[h].kuhu != "0") {
                        return  mCtList->listTooted()[toodeid].alamtooted[m_alamIndex].detailid[h].kuhu;
                    } else {
                        return " ";
                    }

                }

                if (index.row() == ((h + 1)) && index.column() == 6) return mCtList->listTooted()[toodeid].alamtooted[m_alamIndex].detailid[h].tehtud;


            }
        }


        return QVariant("");
    case HeadingRole:
        if (index.row() > -1) {

            if (index.row() == 0) {
                for (int i=0; i < 7; i++) {
                    if (index.column() == i) {

                        return "#e0e0e0";
                    }
                }
            }

            if (index.column() == 6) {
                if (index.row() != 0) {
                    for (int h = 0; h < mCtList->listTooted()[toodeid].alamtooted[m_alamIndex].detailid.size() ; h++) {

                        int tehtud = mCtList->listTooted()[toodeid].alamtooted[m_alamIndex].detailid[h].tehtud;
                        int vaja = mCtList->listTooted()[toodeid].alamtooted[m_alamIndex].detailid[h].tk * mCtList->listTooted()[toodeid].alamtooted[m_alamIndex].alamtootekogus;
                        int juurde = (vaja - mCtList->listTooted()[toodeid].alamtooted[m_alamIndex].detailid[h].pv);
                        int juurde_viis = (int) round(juurde * 0.05 );
                        if (index.row()== (h +1)){
                              if (tehtud <= 0) return "#F6CDE6";
                              if (tehtud > 0 && tehtud < juurde) return "#FFE5BC";
                              if (tehtud >= juurde && tehtud < (juurde + juurde_viis)) return "#DDF9D9";
                              if (tehtud >= (juurde + juurde_viis)) return "#ADD9FE";
                        }

                        //if (tehtud > 0 && tehtud < juurde) return "#FFE5BC";
                        //if (tehtud >= juurde && tehtud < (juurde + juurde_viis)) return "#DDF9D9";
                        //if (tehtud >= (juurde + juurde_viis)) return "#ADD9FE";
                         //mCtList->listTooted()[toodeid].alamtooted[m_alamIndex].detailid[h].tehtud;

                    }
                }
            }
        }
        return "white";
    case TitleRole:
        if (index.row() == 0) {
            return 21;
        } else {
            if (index.column() == 3 || index.column() == 2) {
                return 17;
            }
            return 17;
        }
    case WidthRole:
            if (index.column() == 0) {
                return 100;
            }
            if (index.column() == 1) {
                return 200;
            }
            if (index.column() == 2) {
                return 160;
            }
            if (index.column() == 3) {
                return 150;
            }
            if (index.column() == 4) {
                return 80;
            }
            if (index.column() == 5) {
                return 150;
            }
            if (index.column() == 6) {
                return 80;
            }
            else {
                return 11;
            }
  default:
      break;
  }
    return QVariant();
}

QHash<int, QByteArray> TootedTableViewModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[TableDataRole] = "tabledata";
    roles[HeadingRole] = "heading";
    roles[TitleRole] = "title";
    roles[WidthRole] = "widthrole";
    return roles;
}

void TootedTableViewModel::setSelectedToode(QString toode)
{
    m_selectedToode = toode;

    //qDebug() << "selected toode set: " << toode;
    for (int f = 0; f < mCtList->listTooted().size(); f++) {

        if (mCtList->listTooted()[f].toote_id == toode) {

            tooteid = f;
            //emit dataChanged(createIndex(0,0), createIndex(20, 8), QVector<int>(0));
        }
    }
}

QString TootedTableViewModel::getSelectedToode()
{

    return m_selectedToode;
}

int TootedTableViewModel::getAlamIndex()
{
    return m_alamIndex;
}

void TootedTableViewModel::setAlamIndex(int val)
{
    m_alamIndex = val;
    //qDebug() << "val alam: " << val;
    emit alamIndexChanged();
}

void TootedTableViewModel::setInitToode()
{
    m_selectedToode = mCtList->listTooted()[0].tootenimi;
    tooteid = 0;
    emit dataChanged(createIndex(0,0), createIndex(20, 6), QVector<int>(0));
}

void TootedTableViewModel::setSelToode(int id)
{
    //qDebug() << "s21" << tooteid;
    //qDebug() << "selected toode set: " << toode;
    for (int f = 0; f < mCtList->listTooted().size(); f++) {
        if (mCtList->listTooted()[f].toote_id == id) {
            //qDebug() << "2s" << tooteid;
            tooteid = f;
            emit dataChanged(createIndex(0,0), createIndex(20, 6), QVector<int>(0));
        }
    }
}
void TootedTableViewModel::setCList(DVtableList * customList) {
    mCtList = customList;

    emit dataChanged(createIndex(0,0), createIndex(20,6), QVector<int>(0));

}

QString TootedTableViewModel::getData(int row, QString par)
{

    int ats = mCtList->listTooted()[tooteid].alamtooted[m_alamIndex].detailid.size();

    for (int h = 0; h < ats ; h++) {
        for (int g = 0; g < 6; g++) {

            if (row == (1 + h)) {
                //qDebug() << "getData func, detail: " << mCtList->listTooted()[tooteid].alamtooted[f].detailid[h].dnr;
                if (par == "dnr") return mCtList->listTooted()[tooteid].alamtooted[m_alamIndex].detailid[h].dnr;
                if (par == "detail") return mCtList->listTooted()[tooteid].alamtooted[m_alamIndex].detailid[h].detail;
                if (par == "pkilp") return mCtList->listTooted()[tooteid].alamtooted[m_alamIndex].detailid[h].kilp;
                if (par == "mmoot") return mCtList->listTooted()[tooteid].alamtooted[m_alamIndex].detailid[h].must;
                if (par == "pmoot") return mCtList->listTooted()[tooteid].alamtooted[m_alamIndex].detailid[h].puhas;
                if (par == "tk") return QString::number(mCtList->listTooted()[tooteid].alamtooted[m_alamIndex].detailid[h].tk);
                if (par == "tehtud") return QString::number(mCtList->listTooted()[tooteid].alamtooted[m_alamIndex].detailid[h].tehtud);



                int vaja = mCtList->listTooted()[tooteid].alamtooted[m_alamIndex].detailid[h].tk * mCtList->listTooted()[tooteid].alamtooted[m_alamIndex].alamtootekogus;
                int juurde = (vaja - mCtList->listTooted()[tooteid].alamtooted[m_alamIndex].detailid[h].pv);
                int juurde_viis = (int) round(juurde * 0.05 + mCtList->listTooted()[tooteid].alamtooted[m_alamIndex].detailid[h].lisa_juurde);

                if (par == "kogus") return QString::number(juurde) + QString(" + ") + QString::number(juurde_viis);
                if (par == "id") return QString::number(mCtList->listTooted()[tooteid].alamtooted[m_alamIndex].detailid[h].id);
                if (par == "num_kogus") return QString::number(juurde);
                if (par == "num_kogus_viis") return QString::number(juurde_viis);
            }

        }
    }





    return "none";

}


DVtableList *TootedTableViewModel::cList() const
{
    return mCtList;
}
