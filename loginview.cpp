#include "loginview.h"
#include <QDebug>
#include <QEventLoop>
#include <QHostAddress>
#include <QHostInfo>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>
#include <QNetworkAccessManager>
#include <QNetworkInterface>
#include <QQmlComponent>
#include <QQmlEngine>
#include <QStringListModel>
#include <QUrlQuery>

LoginView::LoginView(QObject *parent) : QObject(parent)
{
     m_pin = "";
}

void LoginView::setUserid(const QString &userid)
{
    m_userid = userid;
}

QString LoginView::userid() const
{
    return m_userid;
}

void LoginView::validateCredentials()
{

    /* Define json data for post request */
    QJsonObject jsondata;
    QByteArray jsonbyte;
    QUrlQuery query;

    jsondata.insert("pin",m_pin);



    /* Define query parameters */
    jsonbyte = QJsonDocument(jsondata).toJson(QJsonDocument::JsonFormat::Compact);
    query.addQueryItem(QString("creds").toUtf8(), QString(jsonbyte).toUtf8() );

    /* connect to the server and get response */
    QByteArray bytes = server.dataPOST("checkCred", query);

    QString str = QString::fromUtf8(bytes.data(), bytes.size());

    if (str == "SUCCESS") {
        qDebug() << "successful login";
        QByteArray bytes1 = server.dataGET("getTootajaName/", m_pin);
        //QString str1 = QString::fromUtf8(bytes1.data(), bytes1.size());

        QJsonDocument document = QJsonDocument::fromJson(bytes1);
        QJsonValue values = document.array().first();
        m_user = values.toObject().value("nimi").toString();
        m_userid = QString::number(values.toObject().value("id").toInt());
        //this->setUserid(values.toObject().value("id").toString());
        qDebug() << "user id" <<  values.toObject().value("id").toInt();
        emit credentialStatusChanged();
    }
    else if (str == "NOTVALID") {
        qDebug() << "not successful login: " << str;
        emit credentialStatusUnSuccessful();
    }
    else {
        qDebug() << "No internet connection";
        emit noInternetConnection();
    }

}

void LoginView::setUser(const QString user) {
    m_user = user;
    qDebug() << "setting user" <<  user;
}
void LoginView::setPin(QString pin) {
    m_pin = pin;
    qDebug() << "setting pin: "<< pin;
}

QString LoginView::user() {
    return m_user;
}
QString LoginView::pin() {
    return m_pin;
}

bool LoginView::setCredentialStatus(bool pin)
{
    qDebug() << "setting credentials";
    validateCredentials();
    return true;
}

void LoginView::replyFinished(QNetworkReply*reply1) {
    //qDebug() << "~error" << reply1->errorString();
    QByteArray bytes = reply1->readAll();
    QString str = QString::fromUtf8(bytes.data(), bytes.size());


    if (str == "SUCCESS") {
        qDebug() << "successful login";
        emit credentialStatusChanged();
    }
    else if (str == "NOTVALID") {
        qDebug() << "not successful login: " << str;
        emit credentialStatusUnSuccessful();
    }
    else {
        qDebug() << "No internet connection";
        emit noInternetConnection();
    }


}





