#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlComponent>
#include <QQmlContext>
#include <QQuickView>
#include "loginview.h"
#include "wblistmodel.h"
#include "workbenchview.h"
#include "wblist.h"
#include "userdata.h"
#include "productlistmodel.h"
#include "productlist.h"
#include "dvtablemodel.h"
#include "dvtablelist.h"
#include "workbenchdata.h"
#include "detaildata.h"
#include "popupwindow.h"
#include "activedata.h"
#include "listtablemodel.h"
#include "tootedtableviewmodel.h"
#include "sissekanne_model.h"
#include "tableview_workhour.h"

#include "model_entrylist.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);
    QQmlApplicationEngine engine;

    /* Declaring data class variables */
    UserData user;
    WorkbenchData workBench;
    DetailData detail;
    PopupWindow popup;
    ActiveData active;

    /* Declaring list variables */
    DVtableList list;
    WBList WBlist;
    ProductList PList;
    EntryList entryList;

    /* Registering list models */
    qmlRegisterType<workbenchListModel>("WBDataModel", 1, 0, "WBListmodel");
    qmlRegisterType<ProductListModel>("ProductListModel", 1, 0, "ProductListmodel");
    //qmlRegisterType<DVTableModel>("app.customTableModel", 0, 1, "CTableModel");
    qmlRegisterType<ListTableModel>("app.ListTableModel", 1, 0, "ListTableModel");
    qmlRegisterType<TootedTableViewModel>("app.DataTableModel", 1, 0, "DataTableModel");
    //qmlRegisterType<ModelEntries>("app.EntriesModel", 1, 0, "EntriesModel");
    qmlRegisterType<EntryListModel>("app.EntryList", 1, 0, "EntryListModel");
    qmlRegisterType<WorkhourModel>("app.WorkhourModel", 1, 0, "WorkhourModel");


    /* Registering uncreatable types: lists */
    qmlRegisterUncreatableType<WBList>("WB", 1, 0, "WBList", QStringLiteral("sth cool here"));
    //qmlRegisterUncreatableType<ProductList>("ProductList", 1, 0, "PList", QStringLiteral("sth cool here"));
    qmlRegisterUncreatableType<DVtableList>("CustomList", 1, 0, "CList", QStringLiteral("sth cool here"));
    qmlRegisterUncreatableType<EntryList>("EntryList", 1, 0, "EntryList", QStringLiteral("EntryList should not be created in QML"));

    /* Registering Models and Views */
    //qmlRegisterType<DVTableModel>("app.model.tableview", 0, 1, "DVTableModel");
    qmlRegisterType<LoginView>("app.login",1,0,"LoginHandler");
    qmlRegisterType<workbenchview>("app.wb",1,0,"WBHandler");
    qmlRegisterType<PopupWindow>("app.popup", 1, 0, "DDStructType");

    /* Defining data classes for qml to use */
    engine.rootContext()->setContextProperty("userdata", &user);
    engine.rootContext()->setContextProperty("workbenchdata", &workBench);
    engine.rootContext()->setContextProperty("detaildata", &detail);
    engine.rootContext()->setContextProperty("activedata", &active);

    /* Defining list models for qml */
    engine.rootContext()->setContextProperty(QStringLiteral("Clist"), &list);
    engine.rootContext()->setContextProperty(QStringLiteral("WBlist"), &WBlist);
    engine.rootContext()->setContextProperty(QStringLiteral("entryList"), &entryList);
    //engine.rootContext()->setContextProperty(QStringLiteral("Plist"), &PList);
    engine.rootContext()->setContextProperty("DDStruct", &popup);

    /* Application loading related stuff */
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
