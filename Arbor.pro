QT += quick core
QT += network
CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Refer to the documentation for the
# deprecated API to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0
#ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android-sources
SOURCES += \
        main.cpp \
    loginview.cpp \
    tableview_workhour.cpp \
    workbenchview.cpp \
    wblist.cpp \
    dataview.cpp \
    wblistmodel.cpp \
    userdata.cpp \
    productlistmodel.cpp \
    productlist.cpp \
    dvtablemodel.cpp \
    dvtablelist.cpp \
    popupwindow.cpp \
    detaildata.cpp \
    workbenchdata.cpp \
    server.cpp \
    activedata.cpp \
    listtablemodel.cpp \
    tootedtableviewmodel.cpp \
    sissekanne_model.cpp \
    model_entrylist.cpp \
    list_entry.cpp


RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    loginview.h \
    tableview_workhour.h \
    workbenchview.h \
    wblist.h \
    dataview.h \
    wblistmodel.h \
    userdata.h \
    productlistmodel.h \
    productlist.h \
    dvtablemodel.h \
    dvtablelist.h \
    popupwindow.h \
    detaildata.h \
    workbenchdata.h \
    server.h \
    activedata.h \
    listtablemodel.h \
    tootedtableviewmodel.h \
    sissekanne_model.h \
    model_entrylist.h \
    list_entry.h


DISTFILES +=
