#ifndef PRODUCTLIST_H
#define PRODUCTLIST_H

#include "server.h"

#include <QNetworkAccessManager>
#include <QObject>

struct ProductListItems {
    QString dnr;
    QString detail;
    QString kilp;
    QString must;
    QString puhas;
    int alamtooteid;
    int alamtootekogus;
    int tk;
    int pv;
    int id;
    int lisa_juurde;
    int tehtud;
};

class ProductList : public QObject
{
    Q_OBJECT
public:
    explicit ProductList(QObject *parent = nullptr);
    QVector<ProductListItems> items() const;
    QNetworkReply *reply;
    QNetworkAccessManager * manager;
    Server server;

signals:

public slots:
    void appendItem(ProductListItems detail);
private:
    QVector<ProductListItems> nItems;
};

#endif // PRODUCTLIST_H
