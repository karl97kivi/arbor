import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.12
import QtGraphicalEffects 1.12

Window {
    visible: true
    width: 1300
    height: 760
    title: qsTr("Arbor")

    property Login logini: loginid
    property Workbench workbenchi: workbenchid
    property DataView dataviewi: dataviewid
    property DetailiPopup dpopup: detailpopupid
    property ImageView imageviewpid: imageview_id
    property WorkhourView workhourView: workhourView_id
    property bool blurvis: false;
    property bool frompopup: false;
    property bool tehtudLabel: false

    // THIS MUST BE TRUE and others false
    Login {
        id: loginid
        visible: true
    }

    Workbench {
        id: workbenchid
        visible: false
    }

    DataView {
        id: dataviewid
        visible: false
    }

    DetailiPopup {
        id: detailpopupid
        visible: false
        radius: 10
    }

    ImageView {
        id: imageview_id
        visible: false
    }

    InternetConnection {
        id: internetconnection_id
        visible: false
        radius: 10
        border.color: "red"
    }

    WorkhourView {
        id: workhourView_id
        visible: false

    }




}


