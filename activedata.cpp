#include "activedata.h"

ActiveData::ActiveData(QObject *parent) : QObject(parent)
{


}

QString ActiveData::getUser_id() const
{
    return m_user_id;
}

void ActiveData::setUser_id(const QString &user_id)
{
    m_user_id = user_id;
}



QString ActiveData::getUserKood()
{
    return m_user_kood;
}

void ActiveData::setUserKood(QString val)
{
    m_user_kood = val;
}

QString ActiveData::getSelectedToodeID()
{
    return m_selectedToodeID;
}

void ActiveData::setSelectedToodeID(QString val)
{
    m_selectedToodeID = val;
}
