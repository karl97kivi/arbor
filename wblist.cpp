#include "wblist.h"
#include <QDebug>
#include <QEventLoop>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QNetworkReply>


WBList::WBList(QObject *parent) : QObject(parent)
{


}

void WBList::appendItem(int id, QString name) {
    gridItems item;
    item.id = id;
    item.name = name;
    nItems.append(item);
}

QVector<gridItems> WBList::items() const
{
    return nItems;
}

void WBList::refreshWBlist(QString kood)
{
    nItems.clear();
    qDebug() << "refreshWBlist";

    if (server.checkInternetConnection() == "OK") {
        QByteArray bytes = server.dataGET("getAllBenchesAndroid", "/" + kood);
        QString str = QString::fromUtf8(bytes.data(), bytes.size());
        qDebug() << str;

        if (str != "EMPTY") {
            QJsonDocument document = QJsonDocument::fromJson(bytes);

            QJsonArray array = document.array();


            foreach (const QJsonValue &v, array) {

                int id = v.toObject().value("ID").toInt();
                QString name = v.toObject().value("NIMI").toString();

                appendItem(id, name);
            }
        }
     }else if (server.checkInternetConnection() == "NOT OK") {
        qDebug() << "No internet connection (refreshWBlist)";
    }



}
