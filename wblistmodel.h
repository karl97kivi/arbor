#ifndef WORKBENCHLISTMODEL_H
#define WORKBENCHLISTMODEL_H

#include <QAbstractListModel>
#include "wblist.h"

class WBList;

class workbenchListModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(WBList *list READ list WRITE setList NOTIFY listChanged)

public:
    explicit workbenchListModel(QObject *parent = nullptr);

    enum {
        IdRole = Qt::UserRole,
        NameRole
    };

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    virtual QHash<int, QByteArray> roleNames() const override;
    WBList *list() const;
    Q_INVOKABLE void testing();

signals:
    void listChanged();

public slots:

    void setList(WBList *list);
private:
    WBList *mList;
};

#endif // WORKBENCHLISTMODEL_H
