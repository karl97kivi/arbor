
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtQuick 2.14

import app.WorkhourModel 1.0

Rectangle {
    id: id_root_workhour
    anchors.fill: parent

    property var buttonClickedName: "Täna";

   Rectangle {
       id: toolbarid
       anchors.top: parent.top
       anchors.right: parent.right
       anchors.left: parent.left
       anchors.bottom: imageviewid.top
       height: 100
       gradient: Gradient {
           GradientStop { position: 0.0; color: "#0097e6" }
           GradientStop { position: 1.0; color: "#B2DFF7" }
       }

       Text {
           anchors.verticalCenter: parent.verticalCenter
           anchors.horizontalCenter: parent.horizontalCenter
           text: buttonClickedName
           font.pixelSize: 30
           color: "white"
       }

       Button {
           id: logoutButton
           anchors.verticalCenter: parent.verticalCenter
           anchors.right: parent.right
           anchors.rightMargin: 24
           text: "Tagasi"

           font.pixelSize: 24
           background: Rectangle {
               id: background_logout
               implicitHeight: 40
               implicitWidth: 100
               border.width: 1
               color: button.down ? "#d6d6d6" : "#f6f6f6"
               border.color: "#C9D9E2"
               radius: 5
           }
           MouseArea {
               anchors.fill: parent
               onPressed: {
                   background_logout.color = "#d6d6d6"
               }
               onReleased: {
                   background_logout.color = "#f6f6f6"
               }
               onClicked: {
                   //activedata.setSelectedToodeID("0");
                   imageview_id.focus = false
                   imageview_id.visible = false
                   detailpopupid.visible = true
                   detailpopupid.focus = true

               }
           }
       }
   }
   Rectangle {
       id: id_workhourView
       anchors.top: toolbarid.bottom
       anchors.right: parent.right
       anchors.left: parent.left
       anchors.bottom: parent.bottom

       // Navigation View
       Rectangle {
           id: id_buttonsView
           anchors.top: id_workhourView.top
           anchors.left: id_workhourView.left
           anchors.bottom: id_workhourView.bottom
           width: 250

           Button {
               id: todayButton
               anchors.verticalCenter: parent.verticalCenter
               anchors.horizontalCenter: parent.horizontalCenter
               anchors.verticalCenterOffset: -100
               //anchors.right: parent.right
               //anchors.rightMargin: 24
               text: "Täna"

               font.pixelSize: 24
               background: Rectangle {
                   id: btnBackground
                   implicitHeight: 40
                   implicitWidth: 200
                   border.width: 1
                   color: todayButton.down ? "#d6d6d6" : "#f6f6f6"
                   border.color: "#C9D9E2"
                   radius: 5
               }
               MouseArea {
                   anchors.fill: parent
                   onPressed: {
                       btnBackground.color = "#d6d6d6"
                   }
                   onReleased: {
                       btnBackground.color = "#f6f6f6"
                   }

                   onClicked: {
                       //activedata.setSelectedToodeID("0");
                       console.log("Button Today clicked\n");
                       buttonClickedName = "Täna";
                   }
               }
           }

           Button {
               id: weekButton
               anchors.verticalCenter: parent.verticalCenter
               anchors.horizontalCenter: parent.horizontalCenter
               anchors.verticalCenterOffset:0
               //anchors.right: parent.right
               //anchors.rightMargin: 24
               text: "Nädala ülevaade"

               font.pixelSize: 24
               background: Rectangle {
                   id: btnBackground_1
                   implicitHeight: 40
                   implicitWidth: 200
                   border.width: 1
                   color: todayButton.down ? "#d6d6d6" : "#f6f6f6"
                   border.color: "#C9D9E2"
                   radius: 5
               }
               MouseArea {
                   anchors.fill: parent
                   onPressed: {
                       btnBackground_1.color = "#d6d6d6"
                   }
                   onReleased: {
                       btnBackground_1.color = "#f6f6f6"
                   }

                   onClicked: {
                       //activedata.setSelectedToodeID("0");
                       console.log("Button Week clicked\n");
                       buttonClickedName = "Nädala ülevaade";
                   }
               }
           }
           Button {
               id: monthButton
               anchors.verticalCenter: parent.verticalCenter
               anchors.horizontalCenter: parent.horizontalCenter
               anchors.verticalCenterOffset: 100
               //anchors.right: parent.right
               //anchors.rightMargin: 24
               text: "Kuu ülevaade"

               font.pixelSize: 24
               background: Rectangle {
                   id: btnBackground_2
                   implicitHeight: 40
                   implicitWidth: 200
                   border.width: 1
                   color: todayButton.down ? "#d6d6d6" : "#f6f6f6"
                   border.color: "#C9D9E2"
                   radius: 5
               }
               MouseArea {
                   anchors.fill: parent
                   onPressed: {
                       btnBackground_2.color = "#d6d6d6"
                   }
                   onReleased: {
                       btnBackground_2.color = "#f6f6f6"
                   }

                   onClicked: {
                       //activedata.setSelectedToodeID("0");
                       console.log("Button Month clicked\n");
                       buttonClickedName = "Kuu ülevaade";
                   }
               }
           }


           color: "white"
       }

       // Table view for work hours
       Rectangle {
           id: id_workhourTable
           anchors.top: id_workhourView.top
           anchors.right: id_workhourView.right
           anchors.left: id_buttonsView.right
           anchors.bottom: id_workhourView.bottom

           Rectangle {
               id: id_wh_header
               anchors.top: id_workhourTable.top
               anchors.left: id_workhourTable.left
               anchors.right: id_workhourTable.right

               height: 40
               color: "#e0e0e0"
               RowLayout {
                   height: 40
                   id: id_hdr_layout
                   spacing: 0

                   Rectangle {
                       width: 200
                       Text {
                           anchors.centerIn: parent
                           text: "Tööpink"
                           font.pixelSize: 22
                       }
                   }
                   Rectangle {
                       width: 200
                       Text {
                           anchors.centerIn: parent
                           text: "Kuupäev"
                           font.pixelSize: 22
                       }
                   }
                   Rectangle {
                       width: 160
                       Text {
                           anchors.centerIn: parent
                           text: "Toode"
                           font.pixelSize: 22
                       }
                   }
                   Rectangle {
                       width: 200
                       Text {
                           anchors.centerIn: parent
                           text: "Detail"
                           font.pixelSize: 22
                       }
                   }
                   Rectangle {
                       width: 100
                       Text {
                           anchors.centerIn: parent
                           text: "Tehtud"
                           font.pixelSize: 22
                       }
                   }
                   Rectangle {
                       width: 100
                       Text {
                           anchors.centerIn: parent
                           text: "Hind (€)"
                           font.pixelSize: 22
                       }
                   }
               }
           }

           TableView {
               id: id_tableview
               anchors.top: id_wh_header.bottom
               anchors.left: id_workhourTable.left
               anchors.right: id_workhourTable.right
               //height: 200
               anchors.bottom: id_wh_footer.top
               //columnSpacing: 1
               rowSpacing: 2
               clip: true

               model: WorkhourModel {
                   id:workHourModel
               }

               delegate: Rectangle {
                   id: tableviewItem
                   implicitWidth: widthrole
                   implicitHeight: 40
                   //border.width: 1
                   //border.color: "#e0e0e0"
                   Rectangle {
                       width: widthrole
                       anchors.top: parent.top
                       height: 1
                       color: "#e0e0e0"
                   }
                   Rectangle {
                       height: 35
                       anchors.verticalCenter: parent.verticalCenter
                       anchors.right: parent.right
                       width: 1
                       color: "#e0e0e0"

                   }

                   Text {
                       text: tabledata
                       font.pixelSize: title
                       anchors.centerIn: parent
                   }
                   color: "white"
               }

           }

           Rectangle {
               id: id_wh_footer

               anchors.left: id_workhourTable.left
               anchors.right: id_workhourTable.right
               anchors.bottom: id_workhourTable.bottom
               height: 50
               color: "#e0e0e0"
               Text {
                   anchors.verticalCenter: parent.verticalCenter
                   anchors.horizontalCenter: parent.horizontalCenter
                   anchors.horizontalCenterOffset: 390
                   text: "15.57€"
                   font.bold: true
                   font.pixelSize: 16
               }
           }

       }
   }
}
