#ifndef DVTABLELIST_H
#define DVTABLELIST_H

#include "server.h"

#include <QNetworkReply>
#include <QObject>
#include <QVector>


struct Detail {
    QString dnr;
    QString detail;
    QString kilp;
    QString must;
    QString puhas;
    QString toopink;
    int lisa_juurde;
    int tk;
    int pv;
    int id;
    int tehtud;
    QString hetkel;
    QString kuhu;
};

struct Alamtoode {
    QString alamtootenimi;
    int alamtootekogus;
    QVector<Detail> detailid;
};

struct Toode {
    int rownr;
    QString toote_id;
    QString tootenimi;
    QVector<Alamtoode> alamtooted;
};
class DVtableList : public QObject
{
    Q_OBJECT
public:
    explicit DVtableList(QObject *parent = nullptr);
    QVector<Toode> listTooted();
    QNetworkReply *reply;
    QNetworkAccessManager * manager;

    QNetworkReply *reply1;
    QNetworkAccessManager * manager1;
    Q_INVOKABLE void refreshTooteData(QString ite);
    Q_INVOKABLE QString getFirstTooteID();

    QString getBenchid(QString );
    void appendList(Toode item);
    Server server;

private:
    QVector<Toode> tooted;
    QString benchID;

};

#endif // DVTABLELIST_H
