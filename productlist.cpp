#include "productlist.h"

#include <QEventLoop>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QNetworkReply>

ProductList::ProductList(QObject *parent) : QObject(parent)
{
    if (server.checkInternetConnection() == "OK") {
        QByteArray bytes = server.dataGET("getAllBenchDetails", "/1");
        QString str = QString::fromUtf8(bytes.data(), bytes.size());

        QJsonDocument document = QJsonDocument::fromJson(bytes);

        QJsonArray array = document.array();


        foreach (const QJsonValue &v, array) {
            ProductListItems newItem;
            newItem.id = v.toObject().value("id").toInt();
            newItem.dnr = v.toObject().value("dnr").toString();
            newItem.detail = v.toObject().value("nimi").toString();
            newItem.kilp = v.toObject().value("kilp").toString();
            newItem.puhas = v.toObject().value("pmoot").toString();
            newItem.must = v.toObject().value("mmoot").toString();
            newItem.tk = v.toObject().value("tk").toInt();
            newItem.pv = v.toObject().value("pv").toInt();
            newItem.lisa_juurde = v.toObject().value("lisa_juurde").toInt();
            QString name = v.toObject().value("NIMI").toString();



            appendItem(newItem);
        }
    }else if (server.checkInternetConnection() == "NOT OK") {
        qDebug() << "No internet connection (productList)";
    }
}

QVector<ProductListItems> ProductList::items() const
{
    return nItems;
}

void ProductList::appendItem(ProductListItems detail)
{
    nItems.append(detail);
}
