#ifndef WBLIST_H
#define WBLIST_H



#include <QNetworkAccessManager>
#include <QObject>
#include "server.h"

struct gridItems {
    int id;
    QString name;
};

class WBList : public QObject
{
    Q_OBJECT
public:
    explicit WBList(QObject *parent = nullptr);
    QVector<gridItems> items() const;

     Server server;
    QNetworkReply *reply;
   QNetworkAccessManager * manager;
   Q_INVOKABLE void refreshWBlist(QString kood);

signals:
    void confirmButtonChanged();
    void selectedBenchChanged();
    void preItemAppended();
    void postItemAppended();

public slots:
    void appendItem(int id, QString name);
private:


    QVector<gridItems> nItems;
};

#endif // WBLIST_H
